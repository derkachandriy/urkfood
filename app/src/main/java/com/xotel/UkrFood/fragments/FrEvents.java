package com.xotel.UkrFood.fragments;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;

import com.xotel.msb.apilib.Api;
import com.xotel.msb.apilib.models.events.Event;
import com.xotel.msb.apilib.responseImpl.ResponseEvents;
import com.xotel.UkrFood.R;
import com.xotel.UkrFood.activities.AcEventInfo;
import com.xotel.UkrFood.adapters.AdExpListViewEvents;
import com.xotel.UkrFood.app.BaseFragment;
import com.xotel.UkrFood.app.ExtraMsg;

import java.util.ArrayList;

/**
 * Created by android on 23.05.16.
 */
public class FrEvents extends BaseFragment implements ExpandableListView.OnChildClickListener {
    private ExpandableListView mListView;
    private ArrayList<ArrayList<Event>> lists;

    public static final FrEvents newInstance() {
        FrEvents frEvents = new FrEvents();

        return frEvents;
    }

    @Override
    protected void onCreateView(LayoutInflater inflater) {
        view = inflater.inflate(R.layout.events, null);
        mListView = (ExpandableListView) findViewById(R.id.expandableListView);
        mListView.setOnChildClickListener(this);
        mListView.setGroupIndicator(null);
    }

    @Override
    public void getData() {
        getApi().getEvents(new Api.EventsCallBack() {
            @Override
            public void onSuccess(ResponseEvents responseEvents) {
                lists = new ArrayList<>();
                for (int i = 0; i < responseEvents.size(); i++) {
                    if (responseEvents.get(i).getCategory() != null && !responseEvents.get(i).getCategory().equals("")) {
                        lists.add(new ArrayList<Event>());
                        lists.get(lists.size() - 1).add(responseEvents.get(i));
                    } else {
                        if (i == 0 && lists.size() == 0) {
                            lists.add(new ArrayList<Event>());
                        }
                        lists.get(lists.size() - 1).add(responseEvents.get(i));
                    }
                }
                initUI();
            }
        });
    }

    protected void initUI() {
        mListView.setAdapter(new AdExpListViewEvents(activity, lists));
        for (int i = 0; i < lists.size(); i++)
            mListView.expandGroup(i);
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        Intent intent = new Intent(activity, AcEventInfo.class);
        intent.putExtra(ExtraMsg.E_MSG_OBJECT_ID, (lists.get(groupPosition).get(childPosition)).getId());
        startActivity(intent);

        return false;
    }
}