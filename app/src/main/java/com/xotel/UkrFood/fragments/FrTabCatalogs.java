package com.xotel.UkrFood.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;

import com.xotel.UkrFood.R;
import com.xotel.UkrFood.adapters.AdTabs;
import com.xotel.UkrFood.app.BaseFragment;
import com.xotel.UkrFood.app.ExtraMsg;
import com.xotel.UkrFood.widgets.PagerSlidingTabStrip;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by android on 23.02.17.
 */
public class FrTabCatalogs extends BaseFragment {
    private ViewPager mViewPager;
    private AdTabs mAdapter;
    private PagerSlidingTabStrip mPagerSlidingTabStrip;
    private List<Fragment> mList = new ArrayList<>();
    private int mGroupId;

    public static final FrTabCatalogs newInstance(String title, int groupId) {
        FrTabCatalogs frTabCatalogs = new FrTabCatalogs();
        Bundle bundle = new Bundle();
        bundle.putString(ExtraMsg.E_MSG_OBJECT, title);
        bundle.putInt(ExtraMsg.E_MSG_OBJECT_ID, groupId);
        frTabCatalogs.setArguments(bundle);

        return frTabCatalogs;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGroupId = getArguments().getInt(ExtraMsg.E_MSG_OBJECT_ID, 0);
    }

    @Override
    protected void onCreateView(LayoutInflater inflater) {
        view = inflater.inflate(R.layout.pages, null);
        setTitle(getArguments().getString(ExtraMsg.E_MSG_OBJECT));
        mViewPager = (android.support.v4.view.ViewPager) findViewById(R.id.view_pager);
        mPagerSlidingTabStrip = (PagerSlidingTabStrip) findViewById(R.id.pagerSlidingTabStrip);
        mPagerSlidingTabStrip.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.action_bar_text_size), getResources().getDisplayMetrics()));

        mList.add(FrCatalogs.newInstance(mGroupId));
        if (mGroupId != 2)
            mList.add(FrFavoriteCatalogs.newInstance(mGroupId));
        else
            mPagerSlidingTabStrip.setVisibility(View.GONE);
        mAdapter = new AdTabs(getSupportFragmentManager(), getActivity(), mList, AdTabs.Tabs.tabCatalogs);
        mViewPager.setAdapter(mAdapter);
        mPagerSlidingTabStrip.setViewPager(mViewPager);
        mPagerSlidingTabStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { /**/ }

            @Override
            public void onPageSelected(int position) {
                mAdapter.updateItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) { /**/ }
        });
    }

    @Override
    protected void getData() { /**/ }
}