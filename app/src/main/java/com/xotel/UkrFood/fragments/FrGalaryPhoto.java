package com.xotel.UkrFood.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xotel.UkrFood.R;
import com.xotel.UkrFood.utils.TypeFaceUtils;
import com.xotel.UkrFood.utils.Util;
import com.xotel.UkrFood.widgets.ZoomImageView;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 01.08.14
 * Time: 9:31
 */
public class FrGalaryPhoto extends Fragment {
    private View mReturnView;
    private String mIconUrl;
    private String mTitle;
    private ZoomImageView mIcon;
    private TextView mTextImageTitle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mReturnView = inflater.inflate(R.layout.gallery_fragment, container, false);
        mIcon = (ZoomImageView) mReturnView.findViewById(R.id.icon);
        mTextImageTitle = (TextView) mReturnView.findViewById(R.id.textImageTitle);
        mTextImageTitle.setTypeface(TypeFaceUtils.get(getActivity(), TypeFaceUtils.Type.robotoRegular));
        initUI();
        return mReturnView;
    }

    private void initUI() {
        mTextImageTitle.setText(mTitle);
        Util.loadImage(getActivity(), mIcon, mIconUrl);
    }

    public static final FrGalaryPhoto newInstance(String iconUrl, String title) {
        FrGalaryPhoto fragment = new FrGalaryPhoto();
        fragment.setIconUrl(iconUrl);
        fragment.setTitle(title);

        return fragment;
    }

    private void setIconUrl(String iconUrl) {
        mIconUrl = iconUrl;
    }

    public void setTitle(String title) {
        mTitle = title;
    }
}