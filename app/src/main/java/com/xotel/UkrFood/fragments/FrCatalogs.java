package com.xotel.UkrFood.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.xotel.msb.apilib.Api;
import com.xotel.msb.apilib.models.Catalog;
import com.xotel.msb.apilib.responseImpl.ResponseCatalogs;
import com.xotel.UkrFood.R;
import com.xotel.UkrFood.activities.AcCatalogInfo;
import com.xotel.UkrFood.adapters.AdCatalogs;
import com.xotel.UkrFood.app.BaseFragment;
import com.xotel.UkrFood.app.ExtraMsg;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 20.08.14
 * Time: 9:37
 */
public class FrCatalogs extends BaseFragment {
    private RecyclerView mRecyclerView;
    private int mGroupId;
    private ArrayList<Catalog> mResponse;

    public static final FrCatalogs newInstance(int groupId) {
        FrCatalogs frCatalogs = new FrCatalogs();
        Bundle bundle = new Bundle();
        bundle.putInt(ExtraMsg.E_MSG_OBJECT_ID, groupId);
        frCatalogs.setArguments(bundle);

        return frCatalogs;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mGroupId = getArguments().getInt(ExtraMsg.E_MSG_OBJECT_ID, 0);
    }

    @Override
    protected void onCreateView(LayoutInflater inflater) {
        view = inflater.inflate(R.layout.catalogs, null);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(activity));
    }

    @Override
    public void getData() {
        final Api api = getApi();
        if (mGroupId == 3) {
            api.getCatalogOnline(mGroupId, new Api.CatalogsCallBack() {
                @Override
                public void onSuccess(final ResponseCatalogs responseCatalogs) {
                    mResponse = responseCatalogs;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            initUI(responseCatalogs);
                        }
                    });
                }
            });
        } else {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    api.getCatalogs(mGroupId, new Api.CatalogsCallBack() {
                        @Override
                        public void onSuccess(final ResponseCatalogs responseCatalogs) {
                            mResponse = responseCatalogs;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    initUI(responseCatalogs);
                                }
                            });
                        }
                    });
                }
            });
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            thread.start();
        }
    }

    protected void initUI(final ArrayList<Catalog> responseCatalogs) {
        mRecyclerView.setAdapter(new AdCatalogs(activity, responseCatalogs, mGroupId, new AdCatalogs.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(activity, AcCatalogInfo.class);
                intent.putExtra(ExtraMsg.E_MSG_OBJECT_ID, (responseCatalogs.get(position).getId()));
                intent.putExtra(ExtraMsg.E_MSG_GROUP_ID, mGroupId);
                startActivity(intent);
            }
        }));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        menu.findItem(R.id.action_search).setVisible(true);
        onSearch(menu.findItem(R.id.action_search));
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void onSearch(MenuItem item) {
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                populateAdapter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }

    private final void populateAdapter(String query) {
        ArrayList<Catalog> catalogs = new ArrayList<>();
        for (int i = 0; i < mResponse.size(); i++) {
            if (mResponse.get(i).getSearchStr().toLowerCase().contains(query.toLowerCase())) {
                catalogs.add(mResponse.get(i));
            }
        }
        initUI(catalogs);
    }
}