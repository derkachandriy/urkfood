package com.xotel.UkrFood.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.xotel.msb.apilib.Api;
import com.xotel.msb.apilib.models.Catalog;
import com.xotel.msb.apilib.responseImpl.ResponseCatalogs;
import com.xotel.UkrFood.R;
import com.xotel.UkrFood.activities.AcCatalogInfo;
import com.xotel.UkrFood.adapters.AdCatalogs;
import com.xotel.UkrFood.app.BaseFragment;
import com.xotel.UkrFood.app.ExtraMsg;
import com.xotel.UkrFood.favoriteUtils.UtilFavorites;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by android on 23.02.17.
 */
public class FrFavoriteCatalogs extends BaseFragment {
    private RecyclerView mRecyclerView;
    private int mGroupId;

    public static final FrFavoriteCatalogs newInstance(int groupId) {
        FrFavoriteCatalogs frFavoriteCatalogs = new FrFavoriteCatalogs();
        Bundle bundle = new Bundle();
        bundle.putInt(ExtraMsg.E_MSG_OBJECT_ID, groupId);
        frFavoriteCatalogs.setArguments(bundle);

        return frFavoriteCatalogs;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGroupId = getArguments().getInt(ExtraMsg.E_MSG_OBJECT_ID, 0);
    }

    @Override
    protected void onCreateView(LayoutInflater inflater) {
        view = inflater.inflate(R.layout.catalogs, null);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(activity));
    }

    @Override
    public void getData() { /**/ }

    @Override
    public void onResume() {
        super.onResume();
        final List<Catalog> favoriteCatalogs = new ArrayList<>();
        try {
            List<Integer> favoriteIds = null;
            if (mGroupId == 3) {
                favoriteIds = UtilFavorites.getParticipants(getActivity());
            } else {
                favoriteIds = UtilFavorites.getCatalogs(getActivity());
            }
            if (favoriteIds != null) {
                final Api api = getApi();
                final List<Integer> finalFavoriteIds = favoriteIds;

                if (mGroupId == 3) {
                    api.getCatalogOnline(mGroupId, new Api.CatalogsCallBack() {
                        @Override
                        public void onSuccess(ResponseCatalogs responseCatalogs) {
                            for (int i = 0; i < responseCatalogs.size(); i++) {
                                if (finalFavoriteIds.contains(responseCatalogs.get(i).getId()))
                                    favoriteCatalogs.add(responseCatalogs.get(i));
                            }
                            initUI(favoriteCatalogs);
                        }
                    });
                } else {

                    final Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            api.getCatalogs(mGroupId, new Api.CatalogsCallBack() {
                                @Override
                                public void onSuccess(final ResponseCatalogs responseCatalogs) {
                                    for (int i = 0; i < responseCatalogs.size(); i++) {
                                        if (finalFavoriteIds.contains(responseCatalogs.get(i).getId()))
                                            favoriteCatalogs.add(responseCatalogs.get(i));
                                    }
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            initUI(favoriteCatalogs);
                                        }
                                    });
                                }
                            });
                        }
                    });
                    thread.join();
                    thread.start();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void initUI(final List<Catalog> responseCatalogs) {
        mRecyclerView.setAdapter(new AdCatalogs(activity, (ArrayList<Catalog>) responseCatalogs, mGroupId, new AdCatalogs.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(activity, AcCatalogInfo.class);
                intent.putExtra(ExtraMsg.E_MSG_OBJECT_ID, (responseCatalogs.get(position).getId()));
                intent.putExtra(ExtraMsg.E_MSG_GROUP_ID, mGroupId);
                startActivity(intent);
            }
        }));
    }
}