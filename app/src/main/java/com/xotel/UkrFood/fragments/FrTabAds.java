package com.xotel.UkrFood.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.LayoutInflater;

import com.xotel.UkrFood.R;
import com.xotel.UkrFood.adapters.AdTabs;
import com.xotel.UkrFood.app.BaseFragment;
import com.xotel.UkrFood.app.ExtraMsg;
import com.xotel.UkrFood.widgets.PagerSlidingTabStrip;
import com.xotel.msb.apilib.models.enums.AdsAction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by android on 30.05.18.
 */

public class FrTabAds extends BaseFragment {
    private ViewPager mViewPager;
    private AdTabs mAdapter;
    private PagerSlidingTabStrip mPagerSlidingTabStrip;
    private List<Fragment> mList = new ArrayList<>();

    public static final FrTabAds newInstance(String title) {
        FrTabAds frTabAds = new FrTabAds();
        Bundle bundle = new Bundle();
        bundle.putString(ExtraMsg.E_MSG_OBJECT, title);
        frTabAds.setArguments(bundle);

        return frTabAds;
    }

    @Override
    protected void onCreateView(LayoutInflater inflater) {
        view = inflater.inflate(R.layout.pages, null);
        setTitle(getArguments().getString(ExtraMsg.E_MSG_OBJECT));
        mViewPager = (android.support.v4.view.ViewPager) findViewById(R.id.view_pager);
        mPagerSlidingTabStrip = (PagerSlidingTabStrip) findViewById(R.id.pagerSlidingTabStrip);
        mPagerSlidingTabStrip.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.action_bar_text_size), getResources().getDisplayMetrics()));

        mList.add(FrAds.newInstance(AdsAction.UNKNOWN));
        mList.add(FrAds.newInstance(AdsAction.BUY));
        mList.add(FrAds.newInstance(AdsAction.SELL));
        mAdapter = new AdTabs(getSupportFragmentManager(), getActivity(), mList, AdTabs.Tabs.tabAds);
        mViewPager.setAdapter(mAdapter);
        mPagerSlidingTabStrip.setViewPager(mViewPager);
        mPagerSlidingTabStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { /**/ }

            @Override
            public void onPageSelected(int position) {
                mAdapter.updateItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) { /**/ }
        });
    }

    @Override
    protected void getData() { /**/ }
}