package com.xotel.UkrFood.fragments;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;

import com.xotel.msb.apilib.Api;
import com.xotel.msb.apilib.models.events.Event;
import com.xotel.msb.apilib.responseImpl.ResponseEvents;
import com.xotel.UkrFood.R;
import com.xotel.UkrFood.activities.AcEventInfo;
import com.xotel.UkrFood.adapters.AdExpListViewEvents;
import com.xotel.UkrFood.app.BaseFragment;
import com.xotel.UkrFood.app.ExtraMsg;
import com.xotel.UkrFood.favoriteUtils.UtilFavorites;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by android on 23.02.17.
 */
public class FrFavoriteEvents extends BaseFragment implements ExpandableListView.OnChildClickListener {
    private ExpandableListView mListView;
    private ArrayList<ArrayList<Event>> lists;

    public static final FrFavoriteEvents newInstance() {
        FrFavoriteEvents frFavoriteEvents = new FrFavoriteEvents();

        return frFavoriteEvents;
    }

    @Override
    protected void onCreateView(LayoutInflater inflater) {
        view = inflater.inflate(R.layout.events, null);
        mListView = (ExpandableListView) findViewById(R.id.expandableListView);
        mListView.setOnChildClickListener(this);
        mListView.setGroupIndicator(null);
    }

    @Override
    public void getData() { /**/ }

    @Override
    public void onResume() {
        super.onResume();
        final List<Event> favoriteEvents = new ArrayList<>();
        try {
            final List<Integer> favoriteIds = UtilFavorites.getEvents(getActivity());
            if (favoriteIds != null) {
                lists = new ArrayList<>();
                getApi().getEvents(new Api.EventsCallBack() {
                    @Override
                    public void onSuccess(final ResponseEvents responseEvents) {
                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                for (int i = 0; i < responseEvents.size(); i++) {
                                    if (favoriteIds.contains(responseEvents.get(i).getId()))
                                        favoriteEvents.add(responseEvents.get(i));
                                }
                                for (int i = 0; i < favoriteEvents.size(); i++) {
                                    if (favoriteEvents.get(i).getCategory() != null && !favoriteEvents.get(i).getCategory().equals("")) {
                                        lists.add(new ArrayList<Event>());
                                        lists.get(lists.size() - 1).add(favoriteEvents.get(i));
                                    } else {
                                        if (i == 0 && lists.size() == 0) {
                                            lists.add(new ArrayList<Event>());
                                        }
                                        lists.get(lists.size() - 1).add(favoriteEvents.get(i));
                                    }
                                }
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        initUI();
                                    }
                                });
                            }
                        });
                        try {
                            thread.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        thread.start();
                    }
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    protected void initUI() {
        mListView.setAdapter(new AdExpListViewEvents(activity, lists));
        for (int i = 0; i < lists.size(); i++)
            mListView.expandGroup(i);
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        Intent intent = new Intent(activity, AcEventInfo.class);
        intent.putExtra(ExtraMsg.E_MSG_OBJECT_ID, (lists.get(groupPosition).get(childPosition)).getId());
        startActivity(intent);

        return false;
    }
}