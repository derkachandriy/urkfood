package com.xotel.UkrFood.fragments;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.GridView;

import com.xotel.UkrFood.R;
import com.xotel.UkrFood.activities.AcGallery;
import com.xotel.UkrFood.adapters.AdGridGallery;
import com.xotel.UkrFood.adapters.AdImage;
import com.xotel.UkrFood.app.BaseFragment;
import com.xotel.UkrFood.app.ExtraMsg;
import com.xotel.msb.apilib.models.Page;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 08.08.14
 * Time: 10:13
 */
public class FrPage extends BaseFragment implements AdapterView.OnItemClickListener {
    protected WebView textDescription;
    private Page mPageInfo;
    private Gallery mGallery;
    private GridView mGridView;

    public static FrPage newInstance(Page page) {
        FrPage fragment = new FrPage();
        fragment.setHotelInfo(page);

        return fragment;
    }

    @Override
    protected void getData() {
        getApi().getPageInfo(mPageInfo.getId(), pageIfo -> {
            mPageInfo = pageIfo;
            try {
                initUI();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void initUI() {
        switch (mPageInfo.getPageType()) {
            case page: {
                findViewById(R.id.layoutPage).setVisibility(View.VISIBLE);
                if (mPageInfo.getDescription() != null && mPageInfo.getDescription().length() > 0) {
                    activity.showDescWeb(textDescription, mPageInfo.getDescription());
                }
                if (mPageInfo.getPhotos().size() != 0) {
                    initGallery();
                }
                break;
            }
            case gallery: {
                mGridView.setVisibility(View.VISIBLE);
                if (mPageInfo.getPhotos() != null && mPageInfo.getPhotos().size() != 0) {
                    mGridView.setAdapter(new AdGridGallery(getActivity(), mPageInfo.getPhotos()));
                    mGridView.setOnItemClickListener(this);
                }
                break;
            }
        }
    }

    public void initGallery() {
        if (mPageInfo.getPhotos() != null && mPageInfo.getPhotos().size() != 0) {
            mGallery.setAdapter(new AdImage(getActivity(), mPageInfo.getPhotos()));
            mGallery.setOnItemClickListener(this);
        }
    }

    @Override
    protected void onCreateView(LayoutInflater inflater) {
        view = inflater.inflate(R.layout.page_fragmnet, null);

        mGridView = (GridView) findViewById(R.id.gridView);
        mGallery = (Gallery) findViewById(R.id.gallery);
        textDescription = (WebView) findViewById(R.id.textDescription);
    }

    private void setHotelInfo(Page page) {
        mPageInfo = page;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(getActivity(), AcGallery.class);
        intent.putExtra(ExtraMsg.E_MSG_CURR_IMAGE, position);
        intent.putExtra(ExtraMsg.E_MSG_ICON_URLS, mPageInfo.getPhotos());
        startActivity(intent);
    }
}