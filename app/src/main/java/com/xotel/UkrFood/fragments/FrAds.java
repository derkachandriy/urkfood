package com.xotel.UkrFood.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.xotel.UkrFood.R;
import com.xotel.UkrFood.activities.AcAdsInfo;
import com.xotel.UkrFood.adapters.AdAds;
import com.xotel.UkrFood.app.BaseFragment;
import com.xotel.UkrFood.app.ExtraMsg;
import com.xotel.framework.network.ServerError;
import com.xotel.msb.apilib.Api;
import com.xotel.msb.apilib.models.Ads;
import com.xotel.msb.apilib.models.enums.AdsAction;

import java.util.ArrayList;

/**
 * Created by android on 30.05.18.
 */

public class FrAds extends BaseFragment {
    private RecyclerView mRecyclerView;
    private AdsAction action;
    private ArrayList<Ads> mResponse;

    public static final FrAds newInstance(AdsAction action) {
        FrAds frAds = new FrAds();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ExtraMsg.E_MSG_OBJECT, action);
        frAds.setArguments(bundle);

        return frAds;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        action = (AdsAction) getArguments().getSerializable(ExtraMsg.E_MSG_OBJECT);
    }

    @Override
    protected void onCreateView(LayoutInflater inflater) {
        view = inflater.inflate(R.layout.catalogs, null);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(activity));
    }

    @Override
    public void getData() {
        activity.application.getApi(activity, false, true).getAdsList(action, new Api.GetAdsListCallBack() {
            @Override
            public void onSuccess(final ArrayList<Ads> arrayList) {
                mResponse = arrayList;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initUI(arrayList);
                    }
                });
            }

            @Override
            public void onFailed(ServerError serverError) {

            }
        });
    }

    protected void initUI(final ArrayList<Ads> arrayList) {
        mRecyclerView.setAdapter(new AdAds(activity, arrayList, 0, new AdAds.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(activity, AcAdsInfo.class);
                intent.putExtra(ExtraMsg.E_MSG_OBJECT_ID, (arrayList.get(position).getId()));
                startActivity(intent);
            }
        }));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        menu.findItem(R.id.action_search).setVisible(true);
        onSearch(menu.findItem(R.id.action_search));
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void onSearch(MenuItem item) {
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                populateAdapter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }

    private final void populateAdapter(String query) {
        ArrayList<Ads> adsArrayList = new ArrayList<>();
        for (int i = 0; i < mResponse.size(); i++) {
            if (mResponse.get(i).getSearchStr().toLowerCase().contains(query.toLowerCase())) {
                adsArrayList.add(mResponse.get(i));
            }
        }
        initUI(adsArrayList);
    }
}