package com.xotel.UkrFood.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.xotel.msb.apilib.Api;
import com.xotel.msb.apilib.models.news.News;
import com.xotel.msb.apilib.responseImpl.ResponseNews;
import com.xotel.UkrFood.R;
import com.xotel.UkrFood.activities.AcNewsInfo;
import com.xotel.UkrFood.adapters.AdNews;
import com.xotel.UkrFood.app.BaseFragment;
import com.xotel.UkrFood.app.ExtraMsg;

import java.util.ArrayList;

/**
 * Created by android on 23.05.16.
 */
public class FrNews extends BaseFragment {
    private RecyclerView mRecyclerView;

    public static FrNews newInstance(String title) {
        FrNews frNews = new FrNews();
        Bundle bundle = new Bundle();
        bundle.putString(ExtraMsg.E_MSG_OBJECT, title);
        frNews.setArguments(bundle);

        return frNews;
    }

    @Override
    protected void onCreateView(LayoutInflater inflater) {
        view = inflater.inflate(R.layout.news, null);
        activity.application.getApi(activity, false, false);
        if (activity.application.hasApiProblem()) {
            activity.application.aliveApiMsg();
        }
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(activity));
        setTitle(getArguments().getString(ExtraMsg.E_MSG_OBJECT));
    }

    @Override
    public void getData() {
        activity.application.getApi(activity, false).getNews(new Api.NewsCallBack() {
            @Override
            public void onSuccess(ResponseNews responseNews, boolean isFromServer) {
                initUI(responseNews, isFromServer);
            }
        });
    }

    protected void initUI(final ArrayList<News> news, final boolean isFromServer) {
        mRecyclerView.setAdapter(new AdNews(activity, news, isFromServer, new AdNews.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(activity, AcNewsInfo.class);
                intent.putExtra(ExtraMsg.E_MSG_OBJECT_ID, news.get(position).getId());
                startActivity(intent);
            }
        }));
    }
}