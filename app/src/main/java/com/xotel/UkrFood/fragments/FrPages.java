package com.xotel.UkrFood.fragments;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.TypedValue;
import android.view.LayoutInflater;

import com.xotel.msb.apilib.Api;
import com.xotel.msb.apilib.models.Page;
import com.xotel.msb.apilib.models.enums.CustomButtonType;
import com.xotel.msb.apilib.responseImpl.ResponsePages;
import com.xotel.UkrFood.R;
import com.xotel.UkrFood.adapters.AdPage;
import com.xotel.UkrFood.app.BaseFragment;
import com.xotel.UkrFood.app.ExtraMsg;
import com.xotel.UkrFood.widgets.PagerSlidingTabStrip;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 08.08.14
 * Time: 9:52
 */
public class FrPages extends BaseFragment {
    private ViewPager mViewPager;
    private List<Page> mList;
    private PagerAdapter mAdapter;
    private PagerSlidingTabStrip mPagerSlidingTabStrip;
    private CustomButtonType mButtonType;

    public static final FrPages newInstance(CustomButtonType buttonType) {
        FrPages frPages = new FrPages();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ExtraMsg.E_MSG_OBJECT, buttonType);
        frPages.setArguments(bundle);

        return frPages;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mButtonType = (CustomButtonType) getArguments().getSerializable(ExtraMsg.E_MSG_OBJECT);
    }

    @Override
    protected void onCreateView(LayoutInflater inflater) {
        view = inflater.inflate(R.layout.pages, null);
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mPagerSlidingTabStrip = (PagerSlidingTabStrip) findViewById(R.id.pagerSlidingTabStrip);
        mPagerSlidingTabStrip.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.action_bar_text_size), getResources().getDisplayMetrics()));
    }

    @Override
    protected void getData() {
        getApi().getPages(new Api.PagesCallBack() {
            @Override
            public void onSuccess(ResponsePages responsePages) {
                mList = responsePages;
                initUI();
            }
        });
    }

    private void initUI() {
        if (mButtonType != CustomButtonType.about)
            for (int i = 0; i < mList.size(); i++) {
                if (!mList.get(i).getPageType().name().equals(mButtonType.name())) {
                    mList.remove(i);
                }
            }
        if (mList.size() > 0)
            setTitle(mList.get(0).getTitle());
        mAdapter = new AdPage(getSupportFragmentManager(), mList);
        mViewPager.setAdapter(mAdapter);
        mPagerSlidingTabStrip.setOnPageChangeListener(pageChangeListener);
        mPagerSlidingTabStrip.setViewPager(mViewPager);
    }

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i2) {
        }

        @Override
        public void onPageScrollStateChanged(int i) {
        }

        @Override
        public void onPageSelected(int i) {
            String retVal;
            try {
                retVal = Html.fromHtml(mList.get(i).getTitle()).toString();
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    retVal = URLDecoder.decode(mList.get(i).getTitle(), "utf-8");
                } catch (UnsupportedEncodingException e1) {
                    retVal = mList.get(i).getTitle();
                    e1.printStackTrace();
                }
            }
            setTitle(retVal);
        }
    };
}