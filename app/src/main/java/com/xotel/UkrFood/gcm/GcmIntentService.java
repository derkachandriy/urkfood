package com.xotel.UkrFood.gcm;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.xotel.UkrFood.R;
import com.xotel.UkrFood.activities.AcMain;
import com.xotel.UkrFood.app.App;
import com.xotel.UkrFood.app.BaseActivity;
import com.xotel.UkrFood.app.ExtraMsg;
import com.xotel.UkrFood.dialogs.DlgNewPush;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 21.09.14
 * Time: 13:01
 */
public class GcmIntentService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    private App mApp;

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            Bundle extras = intent.getExtras();
            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
            String messageType = gcm.getMessageType(intent);
            if (extras != null && !extras.isEmpty() && messageType != null) {
                if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                    sendNotification(extras, "Send error: " + extras.toString());
                } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                    sendNotification(extras, "Deleted messages on server: " + extras.toString());          // If it's a regular GCM message, do some work.
                } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                    sendNotification(extras, extras.getString("message"));
                }
            }
            GcmBroadcastReceiver.completeWakefulIntent(intent);
        }
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(Bundle extras, final String msg) {
        mApp = (App) getApplication();
        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent contentIntent = null;
        String type = extras.getString("type");
        if (type != null) {
            if (type.equalsIgnoreCase("new") && extras.getString("event").equalsIgnoreCase("news")) {
                final Intent intent = new Intent(this, AcMain.class);
                intent.putExtra(ExtraMsg.E_MSG_FLAG, true);
                contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
                try {
                    ((BaseActivity) mApp.currentContext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                new DlgNewPush(mApp.currentContext, getString(R.string.new_push_msg), msg, getString(R.string.go_to_new), true, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    }
                                }).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (type.equalsIgnoreCase("update")) {
                final Intent intent = new Intent(this, AcMain.class);
                intent.putExtra(ExtraMsg.E_MSG_FLAG_UPDATE, true);
                contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
                try {
                    ((BaseActivity) mApp.currentContext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                new DlgNewPush(mApp.currentContext, getString(R.string.update), msg, getString(R.string.refresh), true, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    }
                                }).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (msg != null && !msg.equals("")) {
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.icon)
                    .setAutoCancel(true)
                    .setContentTitle(getText(R.string.app_name))
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                    .setContentText(msg)
                    .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
            if (contentIntent != null)
                mBuilder.setContentIntent(contentIntent);
            mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
        }
    }
}