package com.xotel.UkrFood.favoriteUtils;

import java.io.Serializable;
import java.util.List;

/**
 * Created by android on 22.02.17.
 */
public final class ObjectSerialCatalogs implements Serializable {
    private final List<Integer> catalogsIds;

    public ObjectSerialCatalogs(List<Integer> catalogsIds) {
        this.catalogsIds = catalogsIds;
    }

    public List<Integer> getCatalogs() {
        return catalogsIds;
    }
}