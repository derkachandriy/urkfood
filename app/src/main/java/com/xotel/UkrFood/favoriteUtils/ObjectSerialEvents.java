package com.xotel.UkrFood.favoriteUtils;

import java.io.Serializable;
import java.util.List;

/**
 * Created by android on 22.02.17.
 */
public final class ObjectSerialEvents implements Serializable {
    private final List<Integer> eventsIds;

    public ObjectSerialEvents(List<Integer> eventsIds) {
        this.eventsIds = eventsIds;
    }

    public List<Integer> getEventsIds() {
        return eventsIds;
    }
}