package com.xotel.UkrFood.favoriteUtils;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by android on 22.02.17.
 */
public class UtilFavorites {
    private static final String F_EVENTS = "events";
    private static final String F_CATALOGS = "catalogs";
    private static final String F_BUSINESS_PARTICIPANTS = "participants";

    public static final void saveEvents(Context context, ObjectSerialEvents events) throws IOException {
        FileOutputStream fos = null;
        fos = context.openFileOutput(F_EVENTS, Context.MODE_PRIVATE);
        ObjectOutputStream os = new ObjectOutputStream(fos);
        os.writeObject(events);
        os.close();
        fos.close();
    }

    public static final List<Integer> getEvents(Context context) throws IOException, ClassNotFoundException {
        FileInputStream fis = context.openFileInput(F_EVENTS);
        ObjectInputStream is = new ObjectInputStream(fis);
        ObjectSerialEvents list = (ObjectSerialEvents) is.readObject();
        is.close();
        fis.close();

        return list.getEventsIds();
    }

    public static final void saveEvent(Context context, Integer eventId, SaveListener listener) {
        List<Integer> events = null;
        try {
            events = getEvents(context);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (events == null) {
            events = new ArrayList<>();
            events.add(eventId);
        } else {
            if (!events.contains(eventId)) {
                events.add(eventId);
            } else {
                listener.onAlreadySaved();
                return;
            }
        }
        try {
            saveEvents(context, new ObjectSerialEvents(events));
            listener.onSaveSuccess();
        } catch (IOException e) {
            e.printStackTrace();
            listener.onSaveFailed();
        }
    }

    public static final void deleteEvent(Context context, int eventId, DeleteListener deleteListener) {
        List<Integer> events = null;
        try {
            events = getEvents(context);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (events == null) {
            deleteListener.onDeleteFailed();
        } else {
            if (events.contains(eventId)) {
                events.remove(((Integer) eventId));
                try {
                    saveEvents(context, new ObjectSerialEvents(events));
                    deleteListener.onDeleteSuccess();
                } catch (IOException e) {
                    e.printStackTrace();
                    deleteListener.onDeleteFailed();
                }
            }
        }
    }

    public static final boolean isEventInFavorites(Context context, int eventId) {
        List<Integer> events = null;
        try {
            events = getEvents(context);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (events == null)
            return false;
        else {
            return events.contains(eventId);
        }
    }

    public static final void saveParticipants(Context context, ObjectSerialParticipants participants) throws IOException {
        FileOutputStream fos = null;
        fos = context.openFileOutput(F_BUSINESS_PARTICIPANTS, Context.MODE_PRIVATE);
        ObjectOutputStream os = new ObjectOutputStream(fos);
        os.writeObject(participants);
        os.close();
        fos.close();
    }

    public static final List<Integer> getParticipants(Context context) throws IOException, ClassNotFoundException {
        FileInputStream fis = context.openFileInput(F_BUSINESS_PARTICIPANTS);
        ObjectInputStream is = new ObjectInputStream(fis);
        ObjectSerialParticipants list = (ObjectSerialParticipants) is.readObject();
        is.close();
        fis.close();

        return list.getParticipants();
    }

    public static final void saveParticipants(Context context, Integer participantId, SaveListener listener) {
        List<Integer> participants = null;
        try {
            participants = getParticipants(context);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (participants == null) {
            participants = new ArrayList<>();
            participants.add(participantId);
        } else {
            if (!participants.contains(participantId)) {
                participants.add(participantId);
            } else {
                listener.onAlreadySaved();
                return;
            }
        }
        try {
            saveParticipants(context, new ObjectSerialParticipants(participants));
            listener.onSaveSuccess();
        } catch (IOException e) {
            e.printStackTrace();
            listener.onSaveFailed();
        }
    }

    public static final void deleteParticipants(Context context, int participantId, DeleteListener deleteListener) {
        List<Integer> participants = null;
        try {
            participants = getParticipants(context);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (participants == null) {
            deleteListener.onDeleteFailed();
        } else {
            if (participants.contains(participantId)) {
                participants.remove(((Integer) participantId));
                try {
                    saveParticipants(context, new ObjectSerialParticipants(participants));
                    deleteListener.onDeleteSuccess();
                } catch (IOException e) {
                    e.printStackTrace();
                    deleteListener.onDeleteFailed();
                }
            }
        }
    }

    public static final boolean isParticipantInFavorites(Context context, int participantId) {
        List<Integer> participants = null;
        try {
            participants = getParticipants(context);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (participants == null)
            return false;
        else {
            return participants.contains(participantId);
        }
    }

    public static final void saveCatalogs(Context context, ObjectSerialCatalogs catalogs) throws IOException {
        FileOutputStream fos = null;
        fos = context.openFileOutput(F_CATALOGS, Context.MODE_PRIVATE);
        ObjectOutputStream os = new ObjectOutputStream(fos);
        os.writeObject(catalogs);
        os.close();
        fos.close();
    }

    public static final List<Integer> getCatalogs(Context context) throws IOException, ClassNotFoundException {
        FileInputStream fis = context.openFileInput(F_CATALOGS);
        ObjectInputStream is = new ObjectInputStream(fis);
        ObjectSerialCatalogs list = (ObjectSerialCatalogs) is.readObject();
        is.close();
        fis.close();

        return list.getCatalogs();
    }

    public static final void saveCatalog(Context context, Integer catalogId, SaveListener listener) {
        List<Integer> catalogs = null;
        try {
            catalogs = getCatalogs(context);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (catalogs == null) {
            catalogs = new ArrayList<>();
            catalogs.add(catalogId);
        } else {
            if (!catalogs.contains(catalogId)) {
                catalogs.add(catalogId);
            } else {
                listener.onAlreadySaved();
                return;
            }
        }
        try {
            saveCatalogs(context, new ObjectSerialCatalogs(catalogs));
            listener.onSaveSuccess();
        } catch (IOException e) {
            e.printStackTrace();
            listener.onSaveFailed();
        }
    }

    public static final void deleteCatalog(Context context, int catalogId, DeleteListener deleteListener) {
        List<Integer> catalogs = null;
        try {
            catalogs = getCatalogs(context);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (catalogs == null) {
            deleteListener.onDeleteFailed();
        } else {
            if (catalogs.contains(catalogId)) {
                catalogs.remove(((Integer) catalogId));
                try {
                    saveCatalogs(context, new ObjectSerialCatalogs(catalogs));
                    deleteListener.onDeleteSuccess();
                } catch (IOException e) {
                    e.printStackTrace();
                    deleteListener.onDeleteFailed();
                }
            }
        }
    }

    public static final boolean isCatalogInFavorites(Context context, int catalogId) {
        List<Integer> catalogs = null;
        try {
            catalogs = getCatalogs(context);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (catalogs == null)
            return false;
        else {
            return catalogs.contains(catalogId);
        }
    }

    public static final void clearFavorites(Context context) {
        try {
            context.deleteFile(F_EVENTS);
            context.deleteFile(F_CATALOGS);
            context.deleteFile(F_BUSINESS_PARTICIPANTS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface DeleteListener {
        void onDeleteSuccess();

        void onDeleteFailed();
    }

    public interface SaveListener {
        void onSaveSuccess();

        void onSaveFailed();

        void onAlreadySaved();
    }
}