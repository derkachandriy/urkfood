package com.xotel.UkrFood.favoriteUtils;

import java.io.Serializable;
import java.util.List;

/**
 * Created by android on 16.05.18.
 */

public class ObjectSerialParticipants implements Serializable {
    private final List<Integer> participantsIds;

    public ObjectSerialParticipants(List<Integer> participantsIds) {
        this.participantsIds = participantsIds;
    }

    public List<Integer> getParticipants() {
        return participantsIds;
    }
}