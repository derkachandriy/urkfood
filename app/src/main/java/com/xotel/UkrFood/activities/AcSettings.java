package com.xotel.UkrFood.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.xotel.UkrFood.utils.Util;
import com.xotel.msb.apilib.Api;
import com.xotel.msb.apilib.responseImpl.ResponseLocale;
import com.xotel.UkrFood.R;
import com.xotel.UkrFood.app.BaseActivity;
import com.xotel.UkrFood.dialogs.DlgInfo;
import com.xotel.UkrFood.dialogs.DlgInfoAlert;
import com.xotel.UkrFood.utils.TypeFaceUtils;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 14.09.14
 * Time: 12:41
 */
public class AcSettings extends BaseActivity {
    private TextView mTextLogin;
    private TextView mTextId;
    private Spinner mSpinLang;
    private TextView mTextAppVersion;
    private String[] mLangCodes;
    private boolean mFirstOpenLang = true;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        setTitleUnder(null);
        return true;
    }

    @Override
    protected void onCreateView() {
        setContentView(R.layout.settings);
        setTitle(R.string.start_settings);

        mTextId = (TextView) findViewById(R.id.textId);
        mTextLogin = (TextView) findViewById(R.id.textLogin);
        mSpinLang = (Spinner) findViewById(R.id.spinnerLang);
        mTextAppVersion = (TextView) findViewById(R.id.textViewAppVersion);

        mTextId.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        mTextLogin.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));

        ((TextView) findViewById(R.id.textViewSettings)).setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        ((TextView) findViewById(R.id.textViewLogin)).setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        ((TextView) findViewById(R.id.textViewId)).setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        ((TextView) findViewById(R.id.textViewSett)).setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        ((TextView) findViewById(R.id.textViewLang)).setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        ((TextView) findViewById(R.id.textViewAboutCompany)).setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        ((TextView) findViewById(R.id.textViewAboutAs)).setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        ((TextView) findViewById(R.id.textViewComment)).setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        ((TextView) findViewById(R.id.textViewFacebook)).setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        ((TextView) findViewById(R.id.textViewLinkedIn)).setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        ((TextView) findViewById(R.id.textViewInNetwork)).setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));

        initUI();
    }

    @Override
    protected void getData() {
        getApi(this).getLocales(new Api.LocalesCallBack() {
            @Override
            public void onSuccess(final ResponseLocale responseLocale) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String[] langNames = new String[responseLocale.size()];
                        mLangCodes = new String[responseLocale.size()];
                        for (int i = 0; i < responseLocale.size(); i++) {
                            langNames[i] = responseLocale.get(i).getName();
                            mLangCodes[i] = responseLocale.get(i).getCode();
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(AcSettings.this, R.layout.spinner_item, langNames);
                        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                        mSpinLang.setAdapter(adapter);
                        String lang = coreData.getPreferences().getLang();
                        for (int i = 0; i < mLangCodes.length; ++i) {
                            if (mLangCodes[i].equals(lang)) {
                                mSpinLang.setSelection(i);
                                lang = null;
                                break;
                            }
                        }

                        mSpinLang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {
                                coreData.getPreferences().setLang(mLangCodes[position]);
                                if (!mFirstOpenLang) {
                                    setLang(position);
                                }
                                mFirstOpenLang = false;
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) { /**/ }
                        });
                    }
                });
            }
        });
    }

    private void initUI() {
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            mTextAppVersion.setText(getString(R.string.app_version) + " " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        mTextId.setText(coreData.getPreferences().getUserId());
        mTextLogin.setText(coreData.getPreferences().getUserLogin());
    }

    private final void setLang(int position) {
        Util.onAttachLang(this, mLangCodes[position]);
        new DlgInfo(AcSettings.this, getString(R.string.change_lang_success));
        getApi(this).getSession().setLang(mLangCodes[position]);
        mFirstOpenLang = true;
        onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFirstOpenLang = true;
    }

    public void OnClickAbout(View view) {
        new DlgInfoAlert(this, getString(R.string.settings_about_us)).show();
    }

    public void OnClickFeedBack(View view) {
        final Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"ak@ukrainian-food.org"});
        try {
            startActivity(Intent.createChooser(intent, null));
        } catch (android.content.ActivityNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public void OnClickFacebook(View view) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/UITT.Kyiv/?fref=ts")));
    }

    public void OnClickLinkedIn(View view) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.linkedin.com/company/17945734")));
    }
}