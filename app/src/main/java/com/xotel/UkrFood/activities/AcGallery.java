package com.xotel.UkrFood.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.widget.RelativeLayout;

import com.xotel.msb.apilib.models.Photo;
import com.xotel.UkrFood.R;
import com.xotel.UkrFood.adapters.AdGalleryPhoto;
import com.xotel.UkrFood.app.BaseActivity;
import com.xotel.UkrFood.app.ExtraMsg;
import com.xotel.UkrFood.views.CirclePageIndicator;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 01.08.14
 * Time: 9:44
 */
public class AcGallery extends BaseActivity {
    private ViewPager mViewPager;
    private AdGalleryPhoto mAdapter;
    private List<Photo> mList;
    private CirclePageIndicator mCirclePageIndicator;
    private RelativeLayout mRoot;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery);
        mList = (List<Photo>) getIntent().getSerializableExtra(ExtraMsg.E_MSG_ICON_URLS);
        initUI(getIntent().getIntExtra(ExtraMsg.E_MSG_CURR_IMAGE, 0));
    }

    @Override
    public boolean isActionBar() {
        return false;
    }

    @Override
    protected void onCreateView() { /**/ }

    @Override
    protected void getData() { /**/ }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        for (int i = 0; i < getFragmentManager().getBackStackEntryCount(); i++) {
            FragmentManager manager = ((Fragment) ((AdGalleryPhoto) mViewPager.getAdapter()).getItem(i)).getFragmentManager();
            FragmentTransaction trans = manager.beginTransaction();
            trans.remove((Fragment) ((AdGalleryPhoto) mViewPager.getAdapter()).getItem(i));
            trans.commit();
        }
        bundle.putInt(ExtraMsg.E_MSG_CURRENT_ITEM, mViewPager.getCurrentItem());
    }

    @Override
    public void onRestoreInstanceState(Bundle bundle) {
        mViewPager.setCurrentItem(bundle.getInt(ExtraMsg.E_MSG_CURRENT_ITEM));
    }

    private void initUI(int currImage) {
        if (currImage > mList.size())
            currImage = currImage % mList.size();
        mRoot = (RelativeLayout) findViewById(R.id.root);
        mViewPager = (ViewPager) mRoot.findViewById(R.id.view_pager);
        mCirclePageIndicator = (CirclePageIndicator) mRoot.findViewById(R.id.circle);
        mAdapter = new AdGalleryPhoto(getSupportFragmentManager(), mList);
        mViewPager.setAdapter(mAdapter);

        mViewPager.setOnPageChangeListener(pageChangeListener);
        if (mList.size() > 1)
            mCirclePageIndicator.setViewPager(mViewPager);
        mViewPager.setCurrentItem(currImage);
    }

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i2) {
            mCirclePageIndicator.onPageScrolled(i, v, i2);
        }

        @Override
        public void onPageScrollStateChanged(int i) {
            mCirclePageIndicator.onPageScrollStateChanged(i);
        }

        @Override
        public void onPageSelected(int i) {
            mCirclePageIndicator.onPageSelected(i);
        }
    };
}