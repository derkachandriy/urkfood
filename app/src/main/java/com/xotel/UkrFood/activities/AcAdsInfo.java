package com.xotel.UkrFood.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.TextView;

import com.xotel.UkrFood.R;
import com.xotel.UkrFood.adapters.AdImage;
import com.xotel.UkrFood.app.BaseActivity;
import com.xotel.UkrFood.app.ExtraMsg;
import com.xotel.UkrFood.utils.TypeFaceUtils;
import com.xotel.framework.network.ServerError;
import com.xotel.msb.apilib.Api;
import com.xotel.msb.apilib.models.Ads;
import com.xotel.msb.apilib.models.Photo;

import java.util.List;

/**
 * Created by android on 30.05.18.
 */

public class AcAdsInfo extends BaseActivity {
    private int mObjId;
    private TextView mTextName;
    private WebView textDescription;
    private Ads ads;
    private Gallery mGallery;
    private Button mBtAppoint;
    private Button mBtVisit;
    private Button mBtDeleteVisit;
    private Button mBtMap;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mObjId = getIntent().getIntExtra(ExtraMsg.E_MSG_OBJECT_ID, 0);
    }

    @Override
    public void onCreateView() {
        setContentView(R.layout.catalog_info);

        mGallery = (Gallery) findViewById(R.id.gallery);
        textDescription = (WebView) findViewById(R.id.textDescription);
        mTextName = (TextView) findViewById(R.id.textName);
        mBtAppoint = (Button) findViewById(R.id.buttonAppoint);
        mBtVisit = (Button) findViewById(R.id.buttonVisit);
        mBtMap = (Button) findViewById(R.id.buttonMap);
        mBtDeleteVisit = (Button) findViewById(R.id.buttonDeleteVisit);

        updateButtonVisibility();
        mBtDeleteVisit.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        mBtAppoint.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        mBtVisit.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        mBtMap.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        ((TextView) findViewById(R.id.tvAppoint)).setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));

        mTextName.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
    }

    private final void updateButtonVisibility() {
        mBtDeleteVisit.setVisibility(View.GONE);
        mBtVisit.setVisibility(View.GONE);
        mBtAppoint.setVisibility(View.GONE);
        mBtMap.setVisibility(View.GONE);

    }

    @Override
    public void getData() {
        getApi(this).getAdsInfo(mObjId + "", new Api.GetAdsInfoCallBack() {
            @Override
            public void onSuccess(Ads ads) {
                AcAdsInfo.this.ads = ads;
                initUI();
            }

            @Override
            public void onFailed(ServerError serverError) {

            }
        });
    }

    public void initUI() {
        mTextName.setText(ads.getTitle());
        if (ads.getDescription() != null && ads.getDescription().length() > 0)
            showDescWeb(textDescription, ads.getDescription());
        else
            findViewById(R.id.layoutDescription).setVisibility(View.GONE);
        setTitle(ads.getTitle());
        initGallery(ads.getPhotos());
    }

    public void initGallery(final List<Photo> list) {
        if (list != null && list.size() != 0) {
            mGallery.setAdapter(new AdImage(this, list, false));
            mGallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getApplicationContext(), AcGallery.class);
                    intent.putExtra(ExtraMsg.E_MSG_CURR_IMAGE, position);
                    intent.putExtra(ExtraMsg.E_MSG_ICON_URLS, (java.io.Serializable) list);
                    startActivity(intent);
                }
            });
        }
    }
}