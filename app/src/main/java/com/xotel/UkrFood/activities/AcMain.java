package com.xotel.UkrFood.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.xotel.UkrFood.R;
import com.xotel.UkrFood.app.BaseActivity;
import com.xotel.UkrFood.app.BaseFragment;
import com.xotel.UkrFood.app.ExtraMsg;
import com.xotel.UkrFood.dialogs.DlgInfo;
import com.xotel.UkrFood.dialogs.DlgInfoAlert;
import com.xotel.UkrFood.dialogs.DlgUpdateData;
import com.xotel.UkrFood.favoriteUtils.UtilFavorites;
import com.xotel.UkrFood.fragments.FrNews;
import com.xotel.UkrFood.fragments.FrPages;
import com.xotel.UkrFood.fragments.FrTabAds;
import com.xotel.UkrFood.fragments.FrTabCatalogs;
import com.xotel.UkrFood.fragments.FrTabEvents;
import com.xotel.UkrFood.utils.TypeFaceUtils;
import com.xotel.UkrFood.utils.Util;
import com.xotel.msb.apilib.Api;
import com.xotel.msb.apilib.models.CustomButton;
import com.xotel.msb.apilib.models.HotelInfo;
import com.xotel.msb.apilib.models.enums.CustomButtonType;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class AcMain extends BaseActivity implements View.OnClickListener {
    private FragmentManager mFragmentManager;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private LinearLayout mLayoutLeftButtons;
    private HotelInfo mHotelInfo;
    private boolean mIsNews = false;
    private boolean mIsUpdate = false;

    @Override
    public void onCreate(Bundle savedInstacneState) {
        super.onCreate(savedInstacneState);
        mIsNews = getIntent().getBooleanExtra(ExtraMsg.E_MSG_FLAG, false);
        mIsUpdate = getIntent().getBooleanExtra(ExtraMsg.E_MSG_FLAG_UPDATE, false);
    }

    @Override
    protected void onCreateView() {
        setContentView(R.layout.hotel_info);

        mLayoutLeftButtons = findViewById(R.id.layoutLeftButton);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mFragmentManager = getSupportFragmentManager();
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.countries, R.string.countries);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        if (coreData.getPreferences().isAuthorized())
            findViewById(R.id.buttonLogout).setVisibility(View.VISIBLE);
        else
            findViewById(R.id.buttonLogin).setVisibility(View.VISIBLE);

        ((TextView) findViewById(R.id.textSettings)).setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        ((TextView) findViewById(R.id.textLogin)).setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        ((TextView) findViewById(R.id.textLogout)).setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        ((TextView) findViewById(R.id.textUpdate)).setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        ((TextView) findViewById(R.id.tvUk)).setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        ((TextView) findViewById(R.id.tvEn)).setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));

        findViewById(R.id.buttonSettings).setOnClickListener(this);
        findViewById(R.id.buttonLogin).setOnClickListener(this);
        findViewById(R.id.buttonLogout).setOnClickListener(this);
        findViewById(R.id.buttonUpdate).setOnClickListener(this);
        findViewById(R.id.tvUk).setOnClickListener(this);
        findViewById(R.id.tvEn).setOnClickListener(this);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void getData() {
        getApi(this).getHotelInfo(responseHotelInfo -> {
            mHotelInfo = responseHotelInfo;
            initButtons();
        });
        if (mIsUpdate)
            update(false);
        else
            update(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonSettings: {
                startActivity(new Intent(AcMain.this, AcSettings.class));
                break;
            }
            case R.id.buttonLogin: {
                startActivity(new Intent(AcMain.this, AcAuth.class));
                break;
            }
            case R.id.buttonLogout: {
                coreData.clearPreferences();
                UtilFavorites.clearFavorites(AcMain.this);
                getApi(AcMain.this).getSession().setLang(coreData.getPreferences().getLang());
                try {
                    LoginManager.getInstance().logOut();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                restart();
                break;
            }
            case R.id.buttonUpdate: {
                update(false);
                break;
            }
            case R.id.tvUk: {
                setLang("uk");
                break;
            }
            case R.id.tvEn: {
                setLang("en");
                break;
            }
        }
    }

    private void setLang(String lang) {
        if (!coreData.getPreferences().getLang().equalsIgnoreCase(lang)) {
            coreData.getPreferences().setLang(lang);
            Util.onAttachLang(this, lang);
            new DlgInfo(AcMain.this, getString(R.string.change_lang_success));
            getApi(this).getSession().setLang(lang);
            restart();
        }
    }

    private final void update(final boolean isOnlyCheckVersion) {
        application.getApi(this, !isOnlyCheckVersion, !isOnlyCheckVersion).checkArchiveVersion(this, new Api.ArchiveVersionCallBack() {
            @Override
            public void onHasUpdate(final Api.UpdateCallback updateCallback) {
                findViewById(R.id.imageUpdate).setVisibility(View.VISIBLE);
                new DlgUpdateData(AcMain.this, getString(R.string.need_update_data), (dialogInterface, i) -> updateCallback.update(new Api.LoaderListener() {
                    @Override
                    public void onLoadSuccess() {
                        findViewById(R.id.imageUpdate).setVisibility(View.GONE);
                        new DlgInfoAlert(AcMain.this, getString(R.string.update_data_success)).show();
                    }

                    @Override
                    public void onLoadFailed() {
                        new DlgInfoAlert(AcMain.this, getString(R.string.update_data_failed)).show();
                    }
                })).show();
            }

            @Override
            public void onNoUpdate() {
                findViewById(R.id.imageUpdate).setVisibility(View.GONE);
                if (!isOnlyCheckVersion)
                    new DlgInfoAlert(AcMain.this, getString(R.string.no_update)).show();
            }
        });
    }

    private final void restart() {
        Intent newIntent = new Intent(this, AcMain.class);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(newIntent);
        AcMain.this.finish();
    }

    @Override
    public void onActionBack() {
        if (mDrawerLayout.isDrawerOpen(findViewById(R.id.content_frame2)))
            mDrawerLayout.closeDrawer(findViewById(R.id.content_frame2));
        else
            mDrawerLayout.openDrawer(findViewById(R.id.content_frame2));
    }

    private void initButtons() {
        if (mHotelInfo.getButtonsMain().size() > 0) {
            if (!mIsNews)
                onClickHotelInfoButton(mHotelInfo.getButtonsMain().get(0));
            mDrawerLayout.openDrawer(findViewById(R.id.content_frame2));
        }
        for (int i = 0; i < mHotelInfo.getButtonsMain().size(); i++) {
            CustomButton customButton = mHotelInfo.getButtonsMain().get(i);
            mLayoutLeftButtons.addView(createLeftButton(customButton));
            if (mIsNews && customButton.getButtonType() == CustomButtonType.news)
                onClickHotelInfoButton(customButton);
        }
    }

    public final void onClickHotelInfoButton(CustomButton customButton) {
        switch (customButton.getButtonType()) {
            case items: {
                if (customButton.getCatche() == 1 || !customButton.isActive()) {
                    new DlgInfoAlert(this, getString(R.string.error_403)).show();
                } else {
                    if (customButton.isHasDirect()) {
                        Intent intent = new Intent(this, AcCatalogInfo.class);
                        intent.putExtra(ExtraMsg.E_MSG_OBJECT_ID, customButton.getDirectId());
                        intent.putExtra(ExtraMsg.E_MSG_GROUP_ID, customButton.getGroupId());
                        startActivity(intent);
                    } else {
                        runFragment(FrTabCatalogs.newInstance(customButton.getName(), customButton.getGroupId()));
                    }
                }
                break;
            }
            case gallery:
            case page:
            case about: {
                runFragment(FrPages.newInstance(customButton.getButtonType()));
                break;
            }
            case events: {
                if (customButton.isHasDirect()) {
                    Intent intent = new Intent(this, AcEventInfo.class);
                    intent.putExtra(ExtraMsg.E_MSG_OBJECT_ID, customButton.getDirectId());
                    startActivity(intent);
                } else {
                    runFragment(FrTabEvents.newInstance(customButton.getName()));
                }
                break;
            }
            case news: {
                if (customButton.isHasDirect()) {
                    Intent intent = new Intent(this, AcNewsInfo.class);
                    intent.putExtra(ExtraMsg.E_MSG_OBJECT_ID, customButton.getDirectId());
                    startActivity(intent);
                } else {
                    runFragment(FrNews.newInstance(customButton.getName()));
                }
                break;
            }
            case link: {
                try {
                    if (coreData.getPreferences().getLang().equalsIgnoreCase("en")) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://ufexpo.com.ua/en_GB/invitation/")));
                    } else {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(customButton.getExtInfo())));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
            case schema: {
                rxPermissions
                        .request(new String[]{ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION})
                        .subscribe(granted -> {
                            if (granted) {
                                startActivity(new Intent(this, AcMapBox.class));
                            } else {
                                permissionsDenied();
                            }
                        });
                break;
            }
            case ads: {
                if (customButton.getCatche() == 1 || !customButton.isActive()) {
                    new DlgInfoAlert(this, getString(R.string.networking_error)).show();
                } else {
                    runFragment(FrTabAds.newInstance(customButton.getName()));
                }
                break;
            }
        }
    }

    private final void runFragment(BaseFragment fragment) {
        mFragmentManager.beginTransaction().replace(R.id.content_frame1, fragment).commit();
        if (mDrawerLayout.isDrawerOpen(findViewById(R.id.content_frame2)))
            mDrawerLayout.closeDrawer(findViewById(R.id.content_frame2));
    }

    private LinearLayout createLeftButton(final CustomButton customButton) {
        LinearLayout view = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.left_menu_custom_button, mLayoutLeftButtons, false);
        int tmpId = getResources().getIdentifier("button_" + customButton.getIconNumber(), "drawable", getPackageName());
        ((ImageView) view.findViewById(R.id.imageView)).setImageResource(tmpId);
        TextView textView = view.findViewById(R.id.textView);
        textView.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        textView.setText(customButton.getName());
        view.setOnClickListener(v -> onClickHotelInfoButton(customButton));

        return view;
    }
}