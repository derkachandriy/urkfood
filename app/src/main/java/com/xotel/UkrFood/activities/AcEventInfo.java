package com.xotel.UkrFood.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.TextView;

import com.xotel.msb.apilib.Api;
import com.xotel.msb.apilib.models.Photo;
import com.xotel.msb.apilib.models.events.Event;
import com.xotel.msb.apilib.responseImpl.ResponseEventInfo;
import com.xotel.UkrFood.R;
import com.xotel.UkrFood.adapters.AdImage;
import com.xotel.UkrFood.app.BaseActivity;
import com.xotel.UkrFood.app.ExtraMsg;
import com.xotel.UkrFood.dialogs.DlgInfo;
import com.xotel.UkrFood.utils.TypeFaceUtils;
import com.xotel.UkrFood.favoriteUtils.UtilFavorites;

import java.util.List;

/**
 * Created by android on 23.05.16.
 */
public class AcEventInfo extends BaseActivity implements View.OnClickListener {
    private int mEventId;
    private Event mEvent;
    private Gallery mGallery;
    private TextView mTextName;
    private TextView mTextDate;
    private WebView mTextDescription;
    private Button mBtVisit;
    private Button mBtDeleteVisit;
    private boolean mIsFavorite = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEventId = getIntent().getIntExtra(ExtraMsg.E_MSG_OBJECT_ID, 0);
        mIsFavorite = UtilFavorites.isEventInFavorites(this, mEventId);
    }

    @Override
    protected void onCreateView() {
        setContentView(R.layout.event_info);

        mGallery = (Gallery) findViewById(R.id.gallery);
        mTextName = (TextView) findViewById(R.id.textEventName);
        mTextDate = (TextView) findViewById(R.id.textDate);
        mTextDescription = (WebView) findViewById(R.id.textDescription);
        mBtVisit = (Button) findViewById(R.id.buttonVisit);
        mBtDeleteVisit = (Button) findViewById(R.id.buttonDeleteVisit);
        updateButtonVisibility();
        mTextName.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        mTextDate.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        mBtVisit.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        mBtDeleteVisit.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));

        mBtDeleteVisit.setOnClickListener(this);
        mBtVisit.setOnClickListener(this);
    }

    private final void updateButtonVisibility() {
        if (mIsFavorite) {
            mBtDeleteVisit.setVisibility(View.VISIBLE);
            mBtVisit.setVisibility(View.GONE);
        } else {
            mBtDeleteVisit.setVisibility(View.GONE);
            mBtVisit.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void getData() {
        getApi(this).getEventInfo(mEventId, new Api.EventInfoCallBack() {
            @Override
            public void onSuccess(ResponseEventInfo responseEventInfo) {
                mEvent = responseEventInfo;
                initUI();
            }
        });
    }

    private void initUI() {
        setTitle(mEvent.getTitle());
        mTextName.setText(mEvent.getTitle());
        if (mEvent.getDescription() != null && mEvent.getDescription().length() > 0)
            showDescWeb(mTextDescription, mEvent.getDescription());
        else
            findViewById(R.id.layoutDescription).setVisibility(View.GONE);
        initGallery(mEvent.getPhotos());
        if (mEvent.isLong()) {
            mTextDate.setText(mEvent.getBeginDate() + " " + mEvent.getBeginTime() + "-" + mEvent.getEndDate() + " " + mEvent.getEndTime());
        } else {
            mTextDate.setText(mEvent.getBeginDate() + " " + mEvent.getBeginTime());
        }
    }

    public void initGallery(final List<Photo> list) {
        if (list != null && list.size() != 0) {
            mGallery.setAdapter(new AdImage(this, list, false));
            mGallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getApplicationContext(), AcGallery.class);
                    intent.putExtra(ExtraMsg.E_MSG_CURR_IMAGE, position);
                    intent.putExtra(ExtraMsg.E_MSG_ICON_URLS, (java.io.Serializable) list);
                    startActivity(intent);
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonVisit: {
                UtilFavorites.saveEvent(AcEventInfo.this, mEvent.getId(), new UtilFavorites.SaveListener() {
                    @Override
                    public void onSaveSuccess() {
                        new DlgInfo(AcEventInfo.this, getString(R.string.add_event_success));
                        mIsFavorite = true;
                        updateButtonVisibility();
                    }

                    @Override
                    public void onSaveFailed() {
                        new DlgInfo(AcEventInfo.this, getString(R.string.parser_error));
                    }

                    @Override
                    public void onAlreadySaved() {
                        new DlgInfo(AcEventInfo.this, getString(R.string.already_added));
                    }
                });
                break;
            }
            case R.id.buttonDeleteVisit: {
                UtilFavorites.deleteEvent(AcEventInfo.this, mEvent.getId(), new UtilFavorites.DeleteListener() {
                    @Override
                    public void onDeleteSuccess() {
                        new DlgInfo(AcEventInfo.this, getString(R.string.delete_event_success));
                        mIsFavorite = false;
                        updateButtonVisibility();
                    }

                    @Override
                    public void onDeleteFailed() {
                        new DlgInfo(AcEventInfo.this, getString(R.string.parser_error));
                    }
                });
                break;
            }
        }
    }
}