package com.xotel.UkrFood.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.mapbox.mapboxsdk.annotations.PolygonOptions;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.services.commons.geojson.Feature;
import com.xotel.UkrFood.R;
import com.xotel.UkrFood.adapters.AdPopupMap;
import com.xotel.UkrFood.app.App;
import com.xotel.UkrFood.app.ExtraMsg;
import com.xotel.UkrFood.dialogs.DlgPopupMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by android on 27.02.17.
 */
public class AcMapBox extends AppCompatActivity {
    private MapView mapView;
    private MapboxMap mMapboxMap;
    private String mPlace = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPlace = getIntent().getStringExtra(ExtraMsg.E_MSG_OBJECT_ID);

        MapboxAccountManager.start(this, getString(R.string.access_token));

        setContentView(R.layout.mapbox);

        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);

        mapView.setStyleUrl("asset://UkrFood/style_" + App.coreData.getPreferences().getLang() + ".json");

        mapView.getMapAsync(mapboxMap -> {
            mMapboxMap = mapboxMap;
            mapboxMap.getUiSettings().setLogoEnabled(false);
            mapboxMap.getUiSettings().setCompassEnabled(false);
            mapboxMap.getUiSettings().setRotateGesturesEnabled(false);
            mapboxMap.getUiSettings().setAttributionEnabled(false);

            mapboxMap.setOnMapClickListener(point -> {
                final PointF p = mapboxMap.getProjection().toScreenLocation(point);
                List<Feature> features = mapboxMap.queryRenderedFeatures(p, "units-position");
                if (features.size() != 0) {
                    new DlgPopupMap(AcMapBox.this, features.get(0).getProperties().getAsJsonArray("items"), new AdPopupMap.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int id, int groupId) {
                            Intent intent = new Intent(AcMapBox.this, AcCatalogInfo.class);
                            intent.putExtra(ExtraMsg.E_MSG_OBJECT_ID, id);
                            intent.putExtra(ExtraMsg.E_MSG_GROUP_ID, groupId);
                            startActivity(intent);
                        }
                    }).show();
                }
            });

            if (mPlace != null) {
                final Thread thread = new Thread(() -> {
                    try {
                        JSONArray array = new JSONObject(loadJSONFromAsset()).getJSONArray("features");
                        for (int i = 0; i < array.length(); i++) {
                            final JSONObject json = array.getJSONObject(i);
                            if (json.getJSONObject("properties").getString("place").equals(mPlace)) {
                                final List<LatLng> polygon = new ArrayList<>();
                                JSONArray coordinates = json.getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(0);
                                for (int j = 0; j < coordinates.length(); j++) {
                                    polygon.add(new LatLng(coordinates.getJSONArray(j).getDouble(1), coordinates.getJSONArray(j).getDouble(0)));
                                }
                                runOnUiThread(() -> drawPolygon(polygon));
                                break;
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                });
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                thread.start();
            }
        });
    }

    private final void drawPolygon(List<LatLng> polygon) {
        mMapboxMap.addPolygon(new PolygonOptions()
                .addAll(polygon)
                .alpha(0.7f)
                .strokeColor(Color.parseColor("#000000"))
                .fillColor(Color.parseColor("#FBD726")));

        mMapboxMap.addPolyline(new PolylineOptions()
                .addAll(polygon)
                .width(2)
                .color(Color.parseColor("#000000")));

        CameraPosition position = new CameraPosition.Builder()
                .target(new LatLng(polygon.get(0).getLatitude(), polygon.get(0).getLongitude()))
                .zoom(8.5)
                .build();

        mMapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 2000);
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    private String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("json/places-" + getResources().getInteger(R.integer.hotel_id) + "-" + App.coreData.getPreferences().getLang() + ".json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        return json;
    }
}