package com.xotel.UkrFood.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.xotel.msb.apilib.Api;
import com.xotel.UkrFood.R;
import com.xotel.UkrFood.app.BaseActivity;
import com.xotel.UkrFood.dialogs.DlgInfo;
import com.xotel.UkrFood.utils.AuthMethod;
import com.xotel.UkrFood.utils.TypeFaceUtils;
import com.xotel.UkrFood.utils.Util;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 08.08.14
 * Time: 14:54
 */
public class AcAuth extends BaseActivity implements Api.LoginCallBack, View.OnClickListener, Api.LoginFaceBookCallBack, FacebookCallback<LoginResult> {
    private EditText mEditLogin;
    private EditText mEditPass;
    private EditText mEditName;
    private String mRegId = null;
    private TextView mBtRegPage;
    private TextView mBtLoginPage;
    private Button mBtLogin;
    private Button mBtRestore;
    private Button mBtRegistration;
    private TextView mBtRestorePage;
    private CallbackManager callbackManager;
    private LoginButton loginButton;

    @Override
    public boolean isActionBar() {
        return false;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (application.waitForNet) {
            application.waitForNet = false;
            if (application.hasApiProblem()) {
                application.aliveApiMsg();
            }
        }

        FacebookSdk.sdkInitialize(this.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.auth);

        loginButton = (LoginButton) findViewById(R.id.btLoginFaceBook);
        loginButton.registerCallback(callbackManager, this);

        mBtRestorePage = (TextView) findViewById(R.id.auth_bt_restore_page);
        mEditLogin = (EditText) findViewById(R.id.edit_login);
        mEditPass = (EditText) findViewById(R.id.edit_pass);
        mEditName = (EditText) findViewById(R.id.edit_reg_name);
        mBtRegPage = (TextView) findViewById(R.id.auth_bt_registration_page);
        mBtLoginPage = (TextView) findViewById(R.id.auth_bt_login_page);
        mBtLogin = (Button) findViewById(R.id.auth_bt_login);
        mBtRestore = (Button) findViewById(R.id.auth_bt_restore);
        mBtRegistration = (Button) findViewById(R.id.auth_bt_registration);
        try {
            mEditLogin.setText(coreData.getPreferences().getUserLogin());
        } catch (Exception e) { /**/ }
        mEditName.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        mEditLogin.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        mBtRestorePage.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        mBtRestore.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        mBtRegPage.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        mBtLoginPage.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        mBtLogin.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        mBtRegistration.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));

        mBtRegPage.setOnClickListener(this);
        mBtLoginPage.setOnClickListener(this);
        mBtRestorePage.setOnClickListener(this);

        mEditPass.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (mEditPass.getRight() - mEditPass.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        if (mEditPass.getInputType() == 129)
                            mEditPass.setInputType(InputType.TYPE_CLASS_TEXT);
                        else
                            mEditPass.setInputType(129);

                        return true;
                    }
                }
                return false;
            }
        });
    }

    @Override
    protected void onCreateView() { /**/ }

    @Override
    protected void getData() { /**/ }

    private final boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private void getPushId(final AuthMethod method) {
        if (method != AuthMethod.facebook && !isValidEmail(mEditLogin.getText())) {
            Toast.makeText(this, R.string.illegal_email, Toast.LENGTH_SHORT).show();
            return;
        }
        Util.getIdForPushes(this, new Util.GetRegIdCallBack() {
            @Override
            public void onGetRegIdSuccess(String regId) {
                mRegId = regId;
                nextPage(method);
            }

            @Override
            public void onGetRegIdFailed() {
                nextPage(method);
            }
        });
    }

    private void nextPage(final AuthMethod method) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (method) {
                    case login: {
                        login();
                        break;
                    }
                    case registration: {
                        registration();
                        break;
                    }
                    case facebook: {
                        facebook();
                        break;
                    }
                }
            }
        });
    }

    public void btLoginClick(View v) {
        getPushId(AuthMethod.login);
    }

    public void btRegistrationClick(View v) {
        getPushId(AuthMethod.registration);
    }

    public void btRestoreClick(View v) {
        restorePass();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.auth_bt_registration_page: {
                if (mBtRegPage.getVisibility() == View.VISIBLE) {
                    mBtRegPage.setVisibility(View.GONE);
                    mBtLoginPage.setVisibility(View.VISIBLE);
                    mBtLogin.setVisibility(View.GONE);
                    mBtRestorePage.setVisibility(View.VISIBLE);
                    mBtRestore.setVisibility(View.GONE);
                    mBtRegistration.setVisibility(View.VISIBLE);
                    mEditName.setVisibility(View.VISIBLE);
                    mEditPass.setVisibility(View.VISIBLE);
                    mEditPass.setText("");
                }
                break;
            }
            case R.id.auth_bt_login_page: {
                if (mBtLoginPage.getVisibility() == View.VISIBLE) {
                    mBtRegPage.setVisibility(View.VISIBLE);
                    mBtLoginPage.setVisibility(View.GONE);
                    mBtLogin.setVisibility(View.VISIBLE);
                    mBtRestorePage.setVisibility(View.VISIBLE);
                    mBtRestore.setVisibility(View.GONE);
                    mBtRegistration.setVisibility(View.GONE);
                    mEditName.setVisibility(View.GONE);
                    mEditPass.setVisibility(View.VISIBLE);
                    mEditPass.setText("");
                }
                break;
            }
            case R.id.auth_bt_restore_page: {
                if (mBtRestorePage.getVisibility() == View.VISIBLE) {
                    mBtRestorePage.setVisibility(View.GONE);
                    mBtRestore.setVisibility(View.VISIBLE);
                    mBtRegPage.setVisibility(View.VISIBLE);
                    mBtLoginPage.setVisibility(View.VISIBLE);
                    mBtLogin.setVisibility(View.GONE);
                    mBtRegistration.setVisibility(View.GONE);
                    mEditName.setVisibility(View.GONE);
                    mEditPass.setVisibility(View.GONE);
                    mEditPass.setText("");
                }
                break;
            }
        }
    }

    private void restorePass() {
        if (!isValidEmail(mEditLogin.getText())) {
            Toast.makeText(this, R.string.illegal_email, Toast.LENGTH_SHORT).show();
            return;
        } else {
            getApi(this).restorePass(mEditLogin.getText().toString().trim(), new Api.RestorePassCallBack() {
                @Override
                public void onSuccess() {
                    new DlgInfo(AcAuth.this, getString(R.string.restore_pass_success));
                }
            });
        }
    }

    private void login() {
        getApi(this).login(mRegId, mEditLogin.getText().toString().trim(), mEditPass.getText().toString().trim(), this);
    }

    private void registration() {
        getApi(this).registration(mRegId, mEditLogin.getText().toString().trim(), mEditPass.getText().toString().trim(), mEditName.getText().toString().trim(), new Api.RegistrationCallBack() {
            @Override
            public void onRegistrationSuccess(String accessToken, String login, String id, String userCurrency, String userLocale) {
                savePref(accessToken, login, id, true);
            }
        });
    }

    private void facebook() {
        getApi(this).sendFaceBookData(mRegId, AccessToken.getCurrentAccessToken().getToken(), this);
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        getPushId(AuthMethod.facebook);
    }

    @Override
    public void onCancel() {
        LoginManager.getInstance().logOut();
    }

    @Override
    public void onError(FacebookException error) {
        Toast.makeText(AcAuth.this, R.string.facebook_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLoginSuccess(String accessToken, String login, String id, String userCurrency, String userLocale) {
        savePref(accessToken, login, id, false);
    }

    @Override
    public void onNeedRerequest(String permissions) {
        ArrayList<String> list = new ArrayList<>();
        list.add("public_profile");
        String[] str = permissions.split(",");
        for (int i = 0; i < str.length; i++) {
            list.add(str[i]);
        }
        LoginManager.getInstance().logInWithReadPermissions(AcAuth.this, list);
    }

    public void savePref(String accessToken, String login, String id, final boolean isRegistarion) {
        coreData.getPreferences().setAuthorized(true);
        coreData.getPreferences().setAccessToken(accessToken);
        coreData.getPreferences().setUserId(id);
        coreData.getPreferences().setUserLogin(login);
        coreData.getPreferences().setPropertyRegPushId(mRegId);

        String textMsg = getString(R.string.login_success);
        if (isRegistarion)
            textMsg = getString(R.string.registration_success);
        new DlgInfo(this, textMsg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                if (isRegistarion) {
                    coreData.getPreferences().setIsFirst(true);
                    intent = new Intent(AcAuth.this, AcSplash.class);
                } else
                    intent = new Intent(AcAuth.this, AcMain.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                AcAuth.this.finish();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}