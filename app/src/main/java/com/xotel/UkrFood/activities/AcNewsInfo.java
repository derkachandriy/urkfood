package com.xotel.UkrFood.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.TextView;

import com.xotel.UkrFood.R;
import com.xotel.UkrFood.adapters.AdImage;
import com.xotel.UkrFood.app.BaseActivity;
import com.xotel.UkrFood.app.ExtraMsg;
import com.xotel.UkrFood.utils.TypeFaceUtils;
import com.xotel.msb.apilib.Api;
import com.xotel.msb.apilib.models.Photo;
import com.xotel.msb.apilib.models.news.News;
import com.xotel.msb.apilib.responseImpl.ResponseNewInfo;

import java.util.List;

/**
 * Created by android on 23.05.16.
 */
public class AcNewsInfo extends BaseActivity {
    private int mNewsId;
    private News mNew;
    private Gallery mGallery;
    private TextView mTextName;
    private TextView mTextDate;
    private WebView mTextDescription;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        application.getApi(this, false, false);
        if (application.hasApiProblem())
            application.aliveApiMsg();
        mNewsId = getIntent().getIntExtra(ExtraMsg.E_MSG_OBJECT_ID, 0);
    }

    @Override
    protected void onCreateView() {
        setContentView(R.layout.new_info);

        mGallery = (Gallery) findViewById(R.id.gallery);
        mTextName = (TextView) findViewById(R.id.textEventName);
        mTextDate = (TextView) findViewById(R.id.textDate);
        mTextDescription = (WebView) findViewById(R.id.textDescription);

        mTextName.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        mTextDate.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
    }

    @Override
    protected void getData() {
        application.getApi(this, false).getNewInfo(mNewsId, new Api.NewInfoCallBack() {
            @Override
            public void onSuccess(ResponseNewInfo responseNewInfo, boolean isFromServer) {
                mNew = responseNewInfo;
                initUI(isFromServer, mNew.getResUrl());
            }
        });
    }

    private void initUI(boolean isFromServer, String resUrl) {
        setTitle(mNew.getTitle());
        mTextName.setText(mNew.getTitle());
        if (mNew.getDescription() != null && mNew.getDescription().length() > 0)
            showDescWeb(mTextDescription, mNew.getDescription());
        else
            findViewById(R.id.layoutDescription).setVisibility(View.GONE);
        initGallery(mNew.getPhotos(), isFromServer, resUrl);
        mTextDate.setText(mNew.getDate());
    }

    public void initGallery(final List<Photo> list, boolean isFromServer, String resUrl) {
        if (list != null && list.size() != 0) {
            mGallery.setAdapter(new AdImage(this, list, false, isFromServer, resUrl));
            mGallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getApplicationContext(), AcGallery.class);
                    intent.putExtra(ExtraMsg.E_MSG_CURR_IMAGE, position);
                    intent.putExtra(ExtraMsg.E_MSG_ICON_URLS, (java.io.Serializable) list);
                    startActivity(intent);
                }
            });
        }
    }
}