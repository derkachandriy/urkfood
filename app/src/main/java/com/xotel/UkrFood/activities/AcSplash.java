package com.xotel.UkrFood.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.xotel.UkrFood.R;
import com.xotel.UkrFood.app.BaseActivity;
import com.xotel.UkrFood.dialogs.DlgInfo;
import com.xotel.UkrFood.utils.TypeFaceUtils;
import com.xotel.UkrFood.utils.Util;
import com.xotel.msb.apilib.responseImpl.ResponseLocale;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by android on 03.02.15.
 */
public class AcSplash extends BaseActivity implements View.OnClickListener {
    private Button mBtNext;
    private Spinner mSpinLang;
    private String[] mLangCodes;
    private boolean mFirstOpenLang = true;
    private LinearLayout mLayoutContent;
    private ImageView mImageLogo;
    private ImageView mImageBg;
    private boolean mNeedAnimation = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Util.onAttachLang(this, coreData.getPreferences().getLang());
    }

    @Override
    protected void onCreateView() {
        if (coreData.getPreferences().isFirst()) {
            setContentView(R.layout.splash);

            mBtNext = findViewById(R.id.buttonNext);
            mSpinLang = findViewById(R.id.spinnerLang);
            mLayoutContent = findViewById(R.id.layoutContent);
            mImageLogo = findViewById(R.id.imageLogo);
            mImageBg = findViewById(R.id.imageBackground);

            mBtNext.setOnClickListener(this);

            mBtNext.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
            ((TextView) findViewById(R.id.textViewLang)).setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
            if (mNeedAnimation) {
                Animation animationLogo = AnimationUtils.loadAnimation(this, R.anim.logo_splash_anim);
                animationLogo.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) { /**/ }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        mLayoutContent.startAnimation(AnimationUtils.loadAnimation(AcSplash.this, R.anim.content_splash_anim));
                        mLayoutContent.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) { /**/ }
                });
                mImageLogo.startAnimation(animationLogo);
                mImageLogo.setVisibility(View.VISIBLE);
                mImageBg.setAnimation(AnimationUtils.loadAnimation(this, R.anim.splash_bg_anim));
                findViewById(R.id.horizontalScroll).setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return true;
                    }
                });
            } else {
                mLayoutContent.setVisibility(View.VISIBLE);
                mImageLogo.setVisibility(View.VISIBLE);
            }
        } else
            getPushId();
    }

    @Override
    protected void getData() {
        if (coreData.getPreferences().isFirst())
            getApi(this).getLocales(responseLocale -> initLang(responseLocale));
    }

    @Override
    protected void permissionsDenied() {
        AcSplash.this.finish();
    }

    @Override
    protected String[] getPermissions() {
        return new String[]{READ_PHONE_STATE,
                READ_EXTERNAL_STORAGE,
                WRITE_EXTERNAL_STORAGE};
    }

    private void initLang(ResponseLocale responseLocale) {
        String[] langNames = new String[responseLocale.size()];
        mLangCodes = new String[responseLocale.size()];
        for (int i = 0; i < responseLocale.size(); i++) {
            langNames[i] = responseLocale.get(i).getName();
            mLangCodes[i] = responseLocale.get(i).getCode();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.spinner_item_splash, langNames);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mSpinLang.setAdapter(adapter);
        String lang = coreData.getPreferences().getLang();
        for (int i = 0; i < mLangCodes.length; ++i) {
            if (mLangCodes[i].equals(lang)) {
                mSpinLang.setSelection(i);
                break;
            }
        }

        mSpinLang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {
                coreData.getPreferences().setLang(mLangCodes[position]);
                if (!mFirstOpenLang) {
                    setLang(position);
                }
                mFirstOpenLang = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { /**/ }
        });
    }

    @Override
    public boolean isActionBar() {
        return false;
    }

    private final void nextActivity() {
        coreData.getPreferences().setIsFirst(false);
        Intent intent = new Intent(AcSplash.this, AcMain.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        AcSplash.this.finish();
    }

    private void getPushId() {
        Util.getIdForPushes(this, new Util.GetRegIdCallBack() {
            @Override
            public void onGetRegIdSuccess(final String regId) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getApi(AcSplash.this).initPushes(regId);
                        nextActivity();
                    }
                });
            }

            @Override
            public void onGetRegIdFailed() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        nextActivity();
                    }
                });
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonNext: {
                getPushId();
                break;
            }
        }
    }

    private final void setLang(int position) {
        mNeedAnimation = false;
        Util.onAttachLang(this, mLangCodes[position]);
        new DlgInfo(AcSplash.this, getString(R.string.change_lang_success));
        getApi(this).getSession().setLang(mLangCodes[position]);
        mFirstOpenLang = true;
        onResume();
    }
}