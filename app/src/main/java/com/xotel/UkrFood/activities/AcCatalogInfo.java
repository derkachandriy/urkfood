package com.xotel.UkrFood.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.TextView;

import com.xotel.UkrFood.R;
import com.xotel.UkrFood.adapters.AdImage;
import com.xotel.UkrFood.app.BaseActivity;
import com.xotel.UkrFood.app.ExtraMsg;
import com.xotel.UkrFood.dialogs.DlgInfo;
import com.xotel.UkrFood.dialogs.DlgSendEmail;
import com.xotel.UkrFood.favoriteUtils.UtilFavorites;
import com.xotel.UkrFood.utils.TypeFaceUtils;
import com.xotel.msb.apilib.Api;
import com.xotel.msb.apilib.models.Catalog;
import com.xotel.msb.apilib.models.Message;
import com.xotel.msb.apilib.models.Parameter;
import com.xotel.msb.apilib.models.Photo;
import com.xotel.msb.apilib.models.enums.ParameterType;
import com.xotel.msb.apilib.responseImpl.ResponseCatalogInfo;

import java.util.List;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 21.08.14
 * Time: 8:34
 */
public class AcCatalogInfo extends BaseActivity implements View.OnClickListener, UtilFavorites.DeleteListener, UtilFavorites.SaveListener, Api.CatalogInfoCallBack {
    private int mObjId;
    private int mGroupId;
    private TextView mTextName;
    private TextView mTvAppoint;
    private WebView textDescription;
    private Catalog mCatalog;
    private Gallery mGallery;
    private Button mBtAppoint;
    private Button mBtVisit;
    private Button mBtDeleteVisit;
    private Button mBtMap;
    private boolean mIsFavorite = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mObjId = getIntent().getIntExtra(ExtraMsg.E_MSG_OBJECT_ID, 0);
        mGroupId = getIntent().getIntExtra(ExtraMsg.E_MSG_GROUP_ID, 0);
        if (mGroupId == 3)
            mIsFavorite = UtilFavorites.isParticipantInFavorites(this, mObjId);
        else
            mIsFavorite = UtilFavorites.isCatalogInFavorites(this, mObjId);
    }

    @Override
    public void onCreateView() {
        setContentView(R.layout.catalog_info);

        mGallery = (Gallery) findViewById(R.id.gallery);
        textDescription = (WebView) findViewById(R.id.textDescription);
        mTextName = (TextView) findViewById(R.id.textName);
        mBtAppoint = (Button) findViewById(R.id.buttonAppoint);
        mBtVisit = (Button) findViewById(R.id.buttonVisit);
        mBtMap = (Button) findViewById(R.id.buttonMap);

        mBtDeleteVisit = (Button) findViewById(R.id.buttonDeleteVisit);
        mTvAppoint = (TextView) findViewById(R.id.tvAppoint);

        if (mGroupId != 2) {
            updateButtonVisibility();
            mBtAppoint.setVisibility(View.VISIBLE);
            mTvAppoint.setVisibility(View.VISIBLE);
            mBtAppoint.setOnClickListener(this);
            mBtVisit.setOnClickListener(this);
            mBtDeleteVisit.setOnClickListener(this);
            mBtMap.setOnClickListener(this);
            mBtDeleteVisit.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
            mBtAppoint.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
            mBtVisit.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
            mBtMap.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
            ((TextView) findViewById(R.id.tvAppoint)).setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
        }
        if (mGroupId == 3) {
            mBtMap.setVisibility(View.GONE);
            mBtAppoint.setVisibility(View.GONE);
            mTvAppoint.setVisibility(View.GONE);
        }
        mTextName.setTypeface(TypeFaceUtils.get(this, TypeFaceUtils.Type.robotoRegular));
    }

    @Override
    public void onDeleteSuccess() {
        new DlgInfo(AcCatalogInfo.this, getString(R.string.delete_catalog_success));
        mIsFavorite = false;
        updateButtonVisibility();
    }

    @Override
    public void onDeleteFailed() {
        new DlgInfo(AcCatalogInfo.this, getString(R.string.parser_error));
    }

    @Override
    public void onSaveSuccess() {
        mIsFavorite = true;
        updateButtonVisibility();
        new DlgInfo(AcCatalogInfo.this, getString(R.string.add_catalog_success));
    }

    @Override
    public void onSaveFailed() {
        new DlgInfo(AcCatalogInfo.this, getString(R.string.parser_error));
    }

    @Override
    public void onAlreadySaved() {
        new DlgInfo(AcCatalogInfo.this, getString(R.string.already_added));
    }

    private final void updateButtonVisibility() {
        if (mIsFavorite) {
            mBtDeleteVisit.setVisibility(View.VISIBLE);
            mBtVisit.setVisibility(View.GONE);
        } else {
            mBtDeleteVisit.setVisibility(View.GONE);
            mBtVisit.setVisibility(View.VISIBLE);
        }
        if (mGroupId != 3)
            mBtMap.setVisibility(View.VISIBLE);
    }

    @Override
    public void getData() {
        if (mGroupId == 3) {
            getApi(this).getCatalogInfoOnline(mGroupId, mObjId, this);
        } else {
            getApi(this).getCatalogInfo(mGroupId, mObjId, this);
        }
    }

    @Override
    public void onSuccess(ResponseCatalogInfo responsecatalogInfo) {
        mCatalog = responsecatalogInfo;
        initUI();
    }

    public void initUI() {
        mTextName.setText(mCatalog.getTitle());
        if (mCatalog.getDescription() != null && mCatalog.getDescription().length() > 0)
            showDescWeb(textDescription, mCatalog.getDescription());
        else
            findViewById(R.id.layoutDescription).setVisibility(View.GONE);
        setTitle(mCatalog.getTitle());
        initGallery(mCatalog.getPhotos());
    }

    public void initGallery(final List<Photo> list) {
        if (list != null && list.size() != 0) {
            mGallery.setAdapter(new AdImage(this, list, false));
            mGallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getApplicationContext(), AcGallery.class);
                    intent.putExtra(ExtraMsg.E_MSG_CURR_IMAGE, position);
                    intent.putExtra(ExtraMsg.E_MSG_ICON_URLS, (java.io.Serializable) list);
                    startActivity(intent);
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonAppoint: {
                appointMeeting();
                break;
            }
            case R.id.buttonVisit: {
                if (mGroupId == 3)
                    UtilFavorites.saveParticipants(AcCatalogInfo.this, mCatalog.getId(), this);
                else
                    UtilFavorites.saveCatalog(AcCatalogInfo.this, mCatalog.getId(), this);
                break;
            }
            case R.id.buttonDeleteVisit: {
                if (mGroupId == 3) {
                    UtilFavorites.deleteParticipants(AcCatalogInfo.this, mCatalog.getId(), this);
                } else {
                    UtilFavorites.deleteCatalog(AcCatalogInfo.this, mCatalog.getId(), this);
                }
                break;
            }
            case R.id.buttonMap: {
                rxPermissions
                        .request(new String[]{ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION})
                        .subscribe(granted -> {
                            if (granted) {
                                Intent intent = new Intent(AcCatalogInfo.this, AcMapBox.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                Parameter parameter = mCatalog.getParameter(ParameterType.place);
                                if (parameter != null)
                                    intent.putExtra(ExtraMsg.E_MSG_OBJECT_ID, mCatalog.getParameter(ParameterType.place).getValue());
                                startActivity(intent);
                            } else {
                                permissionsDenied();
                            }
                        });
                break;
            }
        }
    }

    private void appointMeeting() {
        new DlgSendEmail(this, getString(R.string.appoint_meeting), text -> {
            Parameter parameter = mCatalog.getParameter(ParameterType.email);
            getApi(AcCatalogInfo.this).sendMessage(mCatalog.getId() + "", "", text, parameter != null ? parameter.getValue() : "", new Api.SendMessageCallBack() {
                @Override
                public void onSuccess(Message message) {

                }
            });
        }).show();
    }
}