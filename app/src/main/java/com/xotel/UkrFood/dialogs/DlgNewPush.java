package com.xotel.UkrFood.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.xotel.UkrFood.R;

/**
 * Created by android on 25.03.16.
 */
public class DlgNewPush extends AlertDialog {
    public DlgNewPush(Context context, String title, String msg, String posBtName, boolean showNegativeButton, OnClickListener listener) {
        super(context);
        setTitle(title);
        setMessage(msg);
        setCancelable(false);
        if (showNegativeButton)
            setButton(BUTTON_NEGATIVE, context.getString(R.string.close), new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dismiss();
                }
            });
        setButton(BUTTON_POSITIVE, posBtName, listener);
    }
}