package com.xotel.UkrFood.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.xotel.UkrFood.R;

/**
 * Created by android on 01.03.17.
 */
public class DlgInfoAlert extends AlertDialog {
    public DlgInfoAlert(Context context, String textMsg) {
        super(context);
        setMessage(textMsg);
        setButton(BUTTON_POSITIVE, context.getText(R.string.ok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });
    }
}