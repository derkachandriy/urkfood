package com.xotel.UkrFood.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.xotel.UkrFood.R;

/**
 * Created by android on 22.03.17.
 */
public class DlgUpdateData extends AlertDialog {
    public DlgUpdateData(Context context, String textMsg, OnClickListener listener) {
        super(context);
        setMessage(textMsg);
        setButton(BUTTON_NEGATIVE, context.getString(R.string.cancel), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });
        setButton(BUTTON_POSITIVE, context.getText(R.string.refresh), listener);
    }
}