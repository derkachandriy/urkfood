package com.xotel.UkrFood.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.xotel.UkrFood.R;


/**
 * Created by android on 23.07.15.
 */
public class DlgNeedAuthorization extends AlertDialog {
    public DlgNeedAuthorization(Context context, String textMsg, OnClickListener listener) {
        super(context);
        setCancelable(false);
        setMessage(textMsg);
        setButton(BUTTON_POSITIVE, context.getText(R.string.dlg_need_auth_login), listener);
        setButton(BUTTON_NEGATIVE, context.getText(R.string.dlg_need_auth_cancel), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });
    }
}