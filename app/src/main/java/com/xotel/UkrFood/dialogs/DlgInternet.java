package com.xotel.UkrFood.dialogs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.xotel.UkrFood.R;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 25.07.14
 * Time: 11:38
 */
@SuppressLint("ValidFragment")
public class DlgInternet extends AlertDialog {
    public DlgInternet(Context context, final InternetCallBack callBack) {
        super(context);
        setCancelable(false);
        setTitle(R.string.app_net_access_dialog_title);
        setMessage(context.getText(R.string.app_net_access_dialog_message));
        setButton(BUTTON_POSITIVE, context.getText(android.R.string.yes), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
                callBack.onSetupInternetAccess();
            }
        });
        setButton(BUTTON_NEGATIVE, context.getText(android.R.string.no), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
                callBack.onSkipInternetAccess();
            }
        });
    }

    public interface InternetCallBack {
        public void onSkipInternetAccess();
        public void onSetupInternetAccess();
    }
}