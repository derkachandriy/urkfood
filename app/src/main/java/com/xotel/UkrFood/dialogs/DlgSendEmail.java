package com.xotel.UkrFood.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.xotel.UkrFood.R;

/**
 * Created by android on 16.05.18.
 */

public class DlgSendEmail extends AlertDialog {
    public DlgSendEmail(Context context, String msg, final OnClickListener listener) {
        super(context);

        final EditText edittext = new EditText(context);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        edittext.setLayoutParams(lp);
        edittext.setTextColor(context.getResources().getColor(R.color.primary_text));
        setView(edittext);
        setMessage(msg);
        setCancelable(false);
        setButton(BUTTON_NEGATIVE, context.getString(R.string.close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });
        setButton(BUTTON_POSITIVE, context.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onClick(edittext.getText().toString());
            }
        });
    }

    public interface OnClickListener {
        void onClick(String text);
    }
}