package com.xotel.UkrFood.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.xotel.UkrFood.R;
import com.xotel.UkrFood.adapters.AdPopupMap;

import com.google.gson.JsonArray;
import com.xotel.UkrFood.utils.DividerItemDecoration;

/**
 * Created by android on 14.03.17.
 */
public class DlgPopupMap extends AlertDialog {
    public DlgPopupMap(final Context context, final JsonArray arrayItems, final AdPopupMap.OnItemClickListener onItemClickListener) {
        super(context);
        View view = LayoutInflater.from(context).inflate(R.layout.popup_map, null);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(new AdPopupMap(context, arrayItems, onItemClickListener));
        recyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL_LIST));
        setView(view);
    }
}