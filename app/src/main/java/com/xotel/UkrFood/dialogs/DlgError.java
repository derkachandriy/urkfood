package com.xotel.UkrFood.dialogs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.xotel.UkrFood.R;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16.09.14
 * Time: 10:43
 */
@SuppressLint("ValidFragment")
public class DlgError extends AlertDialog {
    public DlgError(Context context, String textMsg) {
        this(context, textMsg, null);
    }

    public DlgError(Context context, String textMsg, OnClickListener listener) {
        super(context);
        setCancelable(false);
        setMessage(textMsg);
        if (listener == null)
            listener = new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dismiss();
                }
            };
        setButton(BUTTON_POSITIVE, context.getText(R.string.ok), listener);
    }
}