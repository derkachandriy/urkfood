package com.xotel.UkrFood.dialogs;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Toast;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16.09.14
 * Time: 10:43
 */
public class DlgInfo {
    private View.OnClickListener mListener;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1: {
                    if (mListener != null)
                        mListener.onClick(null);
                    break;
                }
            }
        }
    };

    public DlgInfo(Context context, String textMsg) {
        this(context, textMsg, null);
    }

    public DlgInfo(Context context, String textMsg, View.OnClickListener listener) {
        mListener = listener;
        Toast.makeText(context, textMsg, Toast.LENGTH_LONG).show();
        mHandler.sendEmptyMessageDelayed(1, 3000);
    }
}