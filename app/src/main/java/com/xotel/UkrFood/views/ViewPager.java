package com.xotel.UkrFood.views;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by android on 20.04.15.
 */
public class ViewPager extends android.support.v4.view.ViewPager {
    private boolean canScroll = true;

    public ViewPager(Context context) {
        super(context);
    }

    public ViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(android.view.MotionEvent ev) {
        if(canScroll)
            return super.onInterceptTouchEvent(ev);
        else
            return false;
    }

    public boolean isCanScroll() {
        return canScroll;
    }

    public void setCanScroll(boolean canScroll) {
        this.canScroll = canScroll;
    }
}