package com.xotel.UkrFood.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.xotel.UkrFood.R;
import com.xotel.UkrFood.utils.TypeFaceUtils;

import com.google.gson.JsonArray;

/**
 * Created by android on 14.03.17.
 */
public class AdPopupMap extends RecyclerView.Adapter<AdPopupMap.ViewHolder> {
    private Context mContext;
    private JsonArray mArrayItems;
    private OnItemClickListener mOnItemClickListener;

    public AdPopupMap(Context context, JsonArray arrayItems, OnItemClickListener onItemClickListener) {
        mContext = context;
        mArrayItems = arrayItems;
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.popup_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textView.setText(((JsonObject) mArrayItems.get(position)).get("name").getAsString());
    }

    @Override
    public int getItemCount() {
        return mArrayItems.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected TextView textView;

        public ViewHolder(View view) {
            super(view);
            textView = (TextView) view.findViewById(R.id.textView);

            textView.setTypeface(TypeFaceUtils.get(mContext, TypeFaceUtils.Type.robotoRegular));
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null)
                mOnItemClickListener.onItemClick(v, ((JsonObject) mArrayItems.get(getPosition())).get("id").getAsInt(), ((JsonObject) mArrayItems.get(getPosition())).get("group_id").getAsInt());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int id, int groupId);
    }
}