package com.xotel.UkrFood.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xotel.UkrFood.R;
import com.xotel.UkrFood.utils.TypeFaceUtils;
import com.xotel.UkrFood.utils.Util;
import com.xotel.msb.apilib.models.Catalog;
import com.xotel.msb.apilib.models.Parameter;
import com.xotel.msb.apilib.models.enums.ParameterType;

import java.util.ArrayList;

/**
 * Created by android on 06.08.15.
 */
public class AdCatalogs<T extends Catalog> extends RecyclerView.Adapter<AdCatalogs.ViewHolder> {
    private ArrayList<T> mCatalogs;
    protected Context context;
    private OnItemClickListener mOnItemClickListener;
    private int mGroupId;

    public AdCatalogs(Context context, ArrayList<T> catalogs, int groupId, OnItemClickListener onItemClickListener) {
        this.context = context;
        mCatalogs = catalogs;
        mGroupId = groupId;
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AdCatalogs.ViewHolder(LayoutInflater.from(context).inflate(R.layout.catalogs_item, null));
    }

    @Override
    public void onBindViewHolder(AdCatalogs.ViewHolder holder, int position) {
        holder.name.setText(mCatalogs.get(position).getTitle());
        holder.icon.setImageBitmap(null);
        if (mGroupId == 3) {
            holder.layoutIcon.setVisibility(View.GONE);
        } else {
            holder.layoutIcon.setVisibility(View.VISIBLE);
            if (mCatalogs.get(position).getMainPic().length() == 0)
                holder.icon.setImageResource(R.drawable.no_image_catalog);
            else
                Util.loadImage(context, holder.icon, mCatalogs.get(position).getMainPic());
        }
        if (mGroupId != 1) {
            if (holder.name.getLineCount() == 2)
                holder.description.setMaxLines(2);
            String description = mCatalogs.get(position).getDescription();
            if (description != null) {
                holder.description.setVisibility(View.VISIBLE);
                holder.description.setText(Html.fromHtml(description));
            } else {
                holder.description.setVisibility(View.GONE);
                holder.description.setText("");
            }
        } else {
            Parameter parameter = mCatalogs.get(position).getParameter(ParameterType.country);
            if (parameter != null) {
                holder.country.setVisibility(View.VISIBLE);
                holder.country.setText(parameter.getValue());
            } else {
                holder.country.setVisibility(View.GONE);
                holder.country.setText("");
            }
        }
    }

    @Override
    public int getItemCount() {
        return mCatalogs.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected ImageView icon;
        protected TextView name;
        protected TextView description;
        protected TextView country;
        protected RelativeLayout layoutIcon;

        public ViewHolder(View view) {
            super(view);
            icon = (ImageView) view.findViewById(R.id.imageView);
            name = (TextView) view.findViewById(R.id.textName);
            description = (TextView) view.findViewById(R.id.textDescription);
            country = (TextView) view.findViewById(R.id.textCountry);
            layoutIcon = (RelativeLayout) view.findViewById(R.id.layoutIcon);

            name.setTypeface(TypeFaceUtils.get(context, TypeFaceUtils.Type.robotoRegular));
            description.setTypeface(TypeFaceUtils.get(context, TypeFaceUtils.Type.robotoRegular));
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null)
                mOnItemClickListener.onItemClick(v, getPosition());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}