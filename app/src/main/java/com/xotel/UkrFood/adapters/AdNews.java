package com.xotel.UkrFood.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.xotel.UkrFood.app.App;
import com.xotel.msb.apilib.models.news.News;
import com.xotel.UkrFood.R;
import com.xotel.UkrFood.utils.TypeFaceUtils;
import com.xotel.UkrFood.utils.Util;

import java.util.ArrayList;

/**
 * Created by android on 16.02.17.
 */
public class AdNews extends RecyclerView.Adapter<AdNews.ViewHolder> {
    private Context mContext;
    private ArrayList<News> mNews;
    private boolean mIsFromServer;
    private OnItemClickListener mOnItemClickListener;

    public AdNews(Context context, ArrayList<News> news, boolean isFromServer, OnItemClickListener onItemClickListener) {
        mContext = context;
        mNews = news;
        mIsFromServer = isFromServer;
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.news_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.date.setText(mNews.get(position).getDate());
        holder.title.setText(mNews.get(position).getTitle());
        holder.icon.setImageBitmap(null);
        if (mNews.get(position).getMainPic().length() == 0)
            holder.icon.setImageResource(R.drawable.no_image_catalog);
        else if (mIsFromServer)
            ImageLoader.getInstance().displayImage(mNews.get(position).getResUrl() + mNews.get(position).getMainPic(), holder.icon);
        else
            Util.loadImage(mContext, holder.icon, mNews.get(position).getMainPic());
    }

    @Override
    public int getItemCount() {
        return mNews.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected ImageView icon;
        protected TextView title;
        protected TextView date;

        public ViewHolder(View view) {
            super(view);
            icon = (ImageView) view.findViewById(R.id.imageView);
            title = (TextView) view.findViewById(R.id.textTitle);
            date = (TextView) view.findViewById(R.id.textDate);

            title.setTypeface(TypeFaceUtils.get(mContext, TypeFaceUtils.Type.robotoRegular));
            date.setTypeface(TypeFaceUtils.get(mContext, TypeFaceUtils.Type.robotoRegular));
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null)
                mOnItemClickListener.onItemClick(v, getPosition());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}