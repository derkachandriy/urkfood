package com.xotel.UkrFood.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.xotel.msb.apilib.models.events.Event;
import com.xotel.UkrFood.R;
import com.xotel.UkrFood.utils.TypeFaceUtils;
import com.xotel.UkrFood.utils.Util;

import java.util.ArrayList;

/**
 * Created by android on 23.05.16.
 */
public class AdExpListViewEvents extends BaseExpandableListAdapter {
    private ArrayList<ArrayList<Event>> mGroups;
    private Context mContext;

    public AdExpListViewEvents(Context context, ArrayList<ArrayList<Event>> groups) {
        mContext = context;
        mGroups = groups;
    }

    @Override
    public int getGroupCount() {
        return mGroups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mGroups.get(groupPosition).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mGroups.get(groupPosition);
    }

    @Override
    public Event getChild(int groupPosition, int childPosition) {
        return mGroups.get(groupPosition).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (mGroups.get(groupPosition).get(0) != null) {
            if (mGroups.get(groupPosition).get(0).getCategory() == null || mGroups.get(groupPosition).get(0).getCategory().trim().length() == 0) {
                convertView = new View(mContext);
            } else {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.simple_exp_list_item, null);
                convertView.setPadding(10, 10, 0, 10);
                TextView textGroup = (TextView) convertView.findViewById(R.id.textView);
                textGroup.setCompoundDrawablesWithIntrinsicBounds(isExpanded ? R.drawable.arrow_top : R.drawable.arrow_bottom, 0, 0, 0);
                textGroup.setCompoundDrawablePadding(10);
                textGroup.setTypeface(TypeFaceUtils.get(mContext, TypeFaceUtils.Type.robotoRegular));
                textGroup.setText(mGroups.get(groupPosition).get(0).getCategory());
            }
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.events_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.name.setText(getChild(groupPosition, childPosition).getTitle());
        if (getChild(groupPosition, childPosition).isLong()) {
            holder.date.setText(getChild(groupPosition, childPosition).getBeginDate() + " " + getChild(groupPosition, childPosition).getBeginTime() + "-" + getChild(groupPosition, childPosition).getEndDate() + " " + getChild(groupPosition, childPosition).getEndTime());
        } else {
            holder.date.setText(getChild(groupPosition, childPosition).getBeginDate() + " " + getChild(groupPosition, childPosition).getBeginTime());
        }
        holder.icon.setImageBitmap(null);
        Util.loadImage(mContext, holder.icon, getChild(groupPosition, childPosition).getMainPic());

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    protected class ViewHolder {
        protected ImageView icon;
        protected TextView name;
        protected TextView date;

        public ViewHolder(View view) {
            icon = (ImageView) view.findViewById(R.id.imageView);
            name = (TextView) view.findViewById(R.id.textName);
            date = (TextView) view.findViewById(R.id.textDate);

            name.setTypeface(TypeFaceUtils.get(mContext, TypeFaceUtils.Type.robotoRegular));
            date.setTypeface(TypeFaceUtils.get(mContext, TypeFaceUtils.Type.robotoRegular));
        }
    }
}