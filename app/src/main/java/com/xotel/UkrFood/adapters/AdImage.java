package com.xotel.UkrFood.adapters;

import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.xotel.UkrFood.R;
import com.xotel.UkrFood.utils.Util;
import com.xotel.msb.apilib.models.Photo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by android on 12.12.14.
 */
public class AdImage extends BaseAdapter {
    private List<Photo> mListPhotos = new ArrayList<>();
    private FragmentActivity mContext;
    private boolean mNeedCrop = true;
    private boolean mIsFromServer;
    private String mResUrl = "";

    public AdImage(FragmentActivity context, List<Photo> list) {
        this(context, list, true);
    }

    public AdImage(FragmentActivity context, List<Photo> list, boolean needCrop) {
        this(context, list, needCrop, false, "");
    }

    public AdImage(FragmentActivity context, List<Photo> list, boolean needCrop, boolean isFromServer, String resUrl) {
        mContext = context;
        mListPhotos = list;
        mNeedCrop = needCrop;
        mIsFromServer = isFromServer;
        mResUrl = resUrl;
    }

    @Override
    public int getCount() {
        return mListPhotos.size();
    }

    @Override
    public Photo getItem(int i) {
        return mListPhotos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View convertVeiw, ViewGroup viewGroup) {
        convertVeiw = LayoutInflater.from(mContext).inflate(R.layout.image_view, null);
        ImageView view = (ImageView) convertVeiw.findViewById(R.id.imageView);
        view.setMinimumHeight((int) (mContext.getWindowManager().getDefaultDisplay().getHeight() * 0.3));
        view.getLayoutParams().height = (int) (mContext.getWindowManager().getDefaultDisplay().getHeight() * 0.3);
        view.setMinimumWidth(mContext.getWindowManager().getDefaultDisplay().getWidth() - 20);
        if (!mNeedCrop)
            view.setScaleType(ImageView.ScaleType.FIT_CENTER);
        if (mIsFromServer)
            ImageLoader.getInstance().displayImage(mResUrl + getItem(i).getFileName(), view);
        else
            Util.loadImage(mContext, view, getItem(i).getFileName());

        return convertVeiw;
    }
}