package com.xotel.UkrFood.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xotel.UkrFood.R;
import com.xotel.UkrFood.utils.TypeFaceUtils;
import com.xotel.UkrFood.utils.Util;
import com.xotel.msb.apilib.models.Ads;
import com.xotel.msb.apilib.models.Parameter;
import com.xotel.msb.apilib.models.enums.ParameterType;

import java.util.ArrayList;

/**
 * Created by android on 09.07.18.
 */

public class AdAds extends RecyclerView.Adapter<AdAds.ViewHolder> {
    private ArrayList<Ads> mAds;
    protected Context context;
    private OnItemClickListener mOnItemClickListener;
    private int mGroupId;

    public AdAds(Context context, ArrayList<Ads> adsArrayList, int groupId, OnItemClickListener onItemClickListener) {
        this.context = context;
        mAds = adsArrayList;
        mGroupId = groupId;
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AdAds.ViewHolder(LayoutInflater.from(context).inflate(R.layout.ads_item, null));
    }

    @Override
    public void onBindViewHolder(AdAds.ViewHolder holder, int position) {
        if (mAds.get(position).getAdsAction() != null)
            switch (mAds.get(position).getAdsAction()) {
                case BUY: {
                    holder.type.setText(R.string.looking_for);
                    break;
                }
                case SELL: {
                    holder.type.setText(R.string.propose);
                    break;
                }
            }
        holder.name.setText(mAds.get(position).getTitle());
        holder.layoutIcon.setVisibility(View.GONE);
        holder.icon.setImageBitmap(null);
        if (mAds.get(position).getMainPic().length() == 0)
            holder.icon.setImageResource(R.drawable.no_image_catalog);
        else
            Util.loadImage(context, holder.icon, mAds.get(position).getMainPic());
        if (mGroupId != 1) {
            if (holder.name.getLineCount() == 2)
                holder.description.setMaxLines(2);
            String description = mAds.get(position).getDescription();
            if (description != null) {
                holder.description.setVisibility(View.VISIBLE);
                holder.description.setText(Html.fromHtml(description));
            } else {
                holder.description.setVisibility(View.GONE);
                holder.description.setText("");
            }
        } else {
            Parameter parameter = mAds.get(position).getParameter(ParameterType.country);
            if (parameter != null) {
                holder.country.setVisibility(View.VISIBLE);
                holder.country.setText(parameter.getValue());
            } else {
                holder.country.setVisibility(View.GONE);
                holder.country.setText("");
            }
        }
    }

    @Override
    public int getItemCount() {
        return mAds.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected ImageView icon;
        protected TextView name;
        protected TextView type;
        protected TextView description;
        protected TextView country;
        protected RelativeLayout layoutIcon;

        public ViewHolder(View view) {
            super(view);
            icon = (ImageView) view.findViewById(R.id.imageView);
            name = (TextView) view.findViewById(R.id.textName);
            type = (TextView) view.findViewById(R.id.textType);
            description = (TextView) view.findViewById(R.id.textDescription);
            country = (TextView) view.findViewById(R.id.textCountry);
            layoutIcon = (RelativeLayout) view.findViewById(R.id.layoutIcon);

            name.setTypeface(TypeFaceUtils.get(context, TypeFaceUtils.Type.robotoRegular));
            type.setTypeface(TypeFaceUtils.get(context, TypeFaceUtils.Type.robotoRegular));
            description.setTypeface(TypeFaceUtils.get(context, TypeFaceUtils.Type.robotoRegular));
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null)
                mOnItemClickListener.onItemClick(v, getPosition());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}