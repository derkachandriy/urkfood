package com.xotel.UkrFood.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.xotel.UkrFood.R;

import java.util.List;

/**
 * Created by android on 04.03.15.
 */
public class AdTabs extends FragmentStatePagerAdapter {
    private List<Fragment> mList;
    private String[] mArray;

    public enum Tabs {
        tabEvents,
        tabCatalogs,
        tabAds
    }

    public AdTabs(FragmentManager fm, Context context, List<Fragment> list, Tabs tab) {
        super(fm);
        mList = list;
        if (tab != null)
            switch (tab) {
                case tabEvents:
                case tabCatalogs: {
                    mArray = context.getResources().getStringArray(R.array.tab_all_selected);
                    break;
                }
                case tabAds: {
                    mArray = context.getResources().getStringArray(R.array.tab_ads);
                    break;
                }
            }
    }

    public void updateItem(int position) {
        mList.get(position).onResume();
    }

    @Override
    public Fragment getItem(int position) {
        return mList.get(position);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (mArray != null)
            return mArray[position];
        else
            return null;
    }
}