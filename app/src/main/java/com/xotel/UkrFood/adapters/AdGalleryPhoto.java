package com.xotel.UkrFood.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.xotel.msb.apilib.models.Photo;
import com.xotel.UkrFood.fragments.FrGalaryPhoto;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 01.08.14
 * Time: 9:28
 */
public class AdGalleryPhoto extends FragmentPagerAdapter {
    private List<Photo> mList;

    public AdGalleryPhoto(FragmentManager fm, List<Photo> mList) {
        super(fm);
        this.mList = mList;
    }

    @Override
    public Fragment getItem(final int i) {
        FrGalaryPhoto fragment = null;
        fragment = FrGalaryPhoto.newInstance(mList.get(i).getFileName(), mList.get(i).getTitle());

        return fragment;
    }

    @Override
    public int getCount() {
        return (mList.size());
    }
}