package com.xotel.UkrFood.adapters;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.xotel.msb.apilib.models.Photo;
import com.xotel.UkrFood.R;
import com.xotel.UkrFood.utils.TypeFaceUtils;
import com.xotel.UkrFood.utils.Util;

import java.util.List;

/**
 * Created by android on 02.04.15.
 */
public class AdGridGallery extends BaseAdapter {
    private List<Photo> mListPhotos;
    private Context mContext;

    public AdGridGallery(Context context, List<Photo> list) {
        mContext = context;
        mListPhotos = list;
    }

    @Override
    public int getCount() {
        return mListPhotos.size();
    }

    @Override
    public Object getItem(int position) {
        return mListPhotos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView;
        ImageView imageView;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.grid_image_view, parent, false);
            convertView.setLayoutParams(new GridView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimension(R.dimen.grid_gallery_item_height), mContext.getResources().getDisplayMetrics())));
        }
        textView = (TextView) convertView.findViewById(R.id.textViewTitle);
        imageView = (ImageView) convertView.findViewById(R.id.imageView);
        textView.setTypeface(TypeFaceUtils.get(mContext, TypeFaceUtils.Type.robotoRegular));
        Util.loadImage(mContext, imageView, mListPhotos.get(position).getFileName());
        textView.setText(mListPhotos.get(position).getTitle());

        return convertView;
    }
}