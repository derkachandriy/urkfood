package com.xotel.UkrFood.adapters;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.text.Html;

import com.xotel.msb.apilib.models.Page;
import com.xotel.UkrFood.fragments.FrPage;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 08.08.14
 * Time: 10:03
 */
public class AdPage extends FragmentStatePagerAdapter {
    private List<Page> mList;

    public AdPage(FragmentManager fm, List<Page> mList) {
        super(fm);
        this.mList = mList;
    }

    @Override
    public FrPage getItem(int i) {
        FrPage fragment = FrPage.newInstance(mList.get(i));
        return fragment;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String retVal;
        try {
            retVal = Html.fromHtml(mList.get(position).getTitle()).toString();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                retVal = URLDecoder.decode(mList.get(position).getTitle(), "utf-8");
            } catch (UnsupportedEncodingException e1) {
                retVal = mList.get(position).getTitle();
                e1.printStackTrace();
            }
        }
        return retVal;
    }
}