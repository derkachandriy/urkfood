package com.xotel.UkrFood.utils;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 17.08.14
 * Time: 11:49
 */

public class TypeFaceUtils {

    public static enum Type {
        robotoMedium("fonts/Roboto-Medium.ttf"),
        robotoRegular("fonts/Roboto-Regular.ttf"),
        robotoLight("fonts/Roboto-Light.ttf"),
        robotoCondensed("fonts/Roboto-Condensed.ttf"),
        robotoBold("fonts/Roboto-Bold.ttf"),
        robotoBlack("fonts/Roboto-Black.ttf"),
        robotoItalic("fonts/Roboto-Italic.ttf");

        private String path;
        Type(String path) {
            this.path = path;
        }
    }

    private static HashMap<Type, Typeface> types;

    public static Typeface get(Context context, Type type) {
        if(types == null) {
            types = new HashMap<Type, Typeface>();
            for(Type t : Type.values()) {
                types.put(t, Typeface.createFromAsset(context.getAssets(), t.path));
            }
        }
        return types.get(type);
    }
}