package com.xotel.UkrFood.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.xotel.UkrFood.R;
import com.xotel.UkrFood.app.BaseActivity;

import java.io.IOException;
import java.util.Locale;

/**
 * Created by android on 01.04.15.
 */
public class Util {
    public Util() {
    }

    public static float dpFromPx(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    public static float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    public static final void onAttachLang(Context context, String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, null);
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static final void sendEmail(Context context, String url) {
        sendEmail(context, url, null);
    }

    public static final void sendEmail(Context context, String url, String email) {
        final Intent intent = new Intent(Intent.ACTION_SEND);
        if (email != null)
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_TEXT, url);
        try {
            context.startActivity(Intent.createChooser(intent, null));
        } catch (android.content.ActivityNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public static void loadImage(Context context, ImageView imageView, String fileName) {
        com.xotel.msb.apilib.utils.Util.loadImage(context, imageView, fileName);
    }

    public static final void getIdForPushes(final BaseActivity activity, final GetRegIdCallBack callBack) {
        String regId = "";
        if (checkPlayServices(activity)) {
            final GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(activity);
            regId = activity.coreData.getPreferences().getPropertyRegPushId();
            if (regId.isEmpty()) {
                new AsyncTask() {
                    protected Object doInBackground(Object... params) {
                        try {
                            final String regId = gcm.register(activity.getString(R.string.sender_id_for_pushes));
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    activity.coreData.getPreferences().setPropertyRegPushId(regId);
                                }
                            });
                            callBack.onGetRegIdSuccess(regId);
                        } catch (IOException ex) {
                            callBack.onGetRegIdFailed();
                            ex.printStackTrace();
                        }
                        return null;
                    }

                    protected void onPostExecute(Object result) { /**/ }
                }.execute(null, null, null);
            } else
                callBack.onGetRegIdSuccess(regId);
        } else
            callBack.onGetRegIdFailed();
    }

    public interface GetRegIdCallBack {
        void onGetRegIdSuccess(String regId);

        void onGetRegIdFailed();
    }

    public static boolean checkPlayServices(Activity activity) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, activity, 9000).show();
            } else {
                activity.finish();
            }
            return false;
        }
        return true;
    }
}