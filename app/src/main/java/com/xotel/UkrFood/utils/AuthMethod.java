package com.xotel.UkrFood.utils;

/**
 * Created by android on 18.06.15.
 */
public enum AuthMethod {
    registration,
    login,
    facebook
}