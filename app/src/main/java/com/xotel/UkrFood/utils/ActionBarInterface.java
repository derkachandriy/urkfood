package com.xotel.UkrFood.utils;

/**
 * Created by android on 02.12.14.
 */
public interface ActionBarInterface {
    void setTitleUnder(String textTitleUnder);

    void onActionBack();
}