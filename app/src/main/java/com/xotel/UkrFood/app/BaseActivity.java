package com.xotel.UkrFood.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.tbruyelle.rxpermissions2.RxPermissions;
import com.xotel.UkrFood.R;
import com.xotel.UkrFood.utils.ActionBarInterface;
import com.xotel.msb.apilib.Api;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 25.07.14
 * Time: 13:51
 */
public abstract class BaseActivity extends AppCompatActivity implements ActionBarInterface {
    public App application;
    protected String currLang = null;
    protected boolean onResumeFlag = false;
    public CoreData coreData;
    private ProgressDialog progressDialog;
    protected final RxPermissions rxPermissions = new RxPermissions(this);

    public Api getApi(Context context) {
        return application.getApi(context);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        if (isActionBar()) {
            setSupportActionBar(findViewById(R.id.toolbar));
            initActionBar();
        }
    }

    public boolean isActionBar() {
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanseState) {
        super.onCreate(savedInstanseState);
        application = (App) getApplication();
        coreData = App.coreData;
    }

    public void exit() {
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    public void initActionBar() {
        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
            bar.setHomeButtonEnabled(true);
            bar.setDisplayShowHomeEnabled(true);
        }
    }

    public void showDescWeb(final WebView webView, String text) {
        webView.getSettings().setJavaScriptEnabled(false);
        webView.setInitialScale(200);
        String htmlString = "<html><head><style>iframe {max-width: 100%; width:auto; height: auto;}</style></head><body>" + text + "</body></html>";
        if (text.length() > 0) {
            webView.loadData(htmlString, "text/html; charset=UTF-8", null);
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {

                }
            });
        } else
            webView.setVisibility(View.GONE);
        webView.setBackgroundColor(Color.TRANSPARENT);
    }

    protected String[] getPermissions() {
        return null;
    }

    protected void permissionsDenied() {

    }

    @Override
    public void onResume() {
        super.onResume();
        application.currentContext = this;
        //need to change lang
        if (!onResumeFlag || !App.coreData.getPreferences().getLang().equals(currLang)) {
            onResumeFlag = true;
            currLang = App.coreData.getPreferences().getLang();
            if (getPermissions() != null) {
                rxPermissions
                        .request(getPermissions())
                        .subscribe(granted -> {
                            if (granted) {
                                postResume();
                            } else {
                                permissionsDenied();
                            }
                        });
            } else {
                postResume();
            }
        }
    }

    private void postResume() {
        onCreateView();
        getData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        onResumeFlag = false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onActionBack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActionBack() {
        onBackPressed();
    }

    @Override
    public void setTitleUnder(String textTitleUnder) {
        if (getSupportActionBar() != null) {
            if (textTitleUnder == null)
                getSupportActionBar().setSubtitle(textTitleUnder);
            else
                getSupportActionBar().setSubtitle((Html.fromHtml("<font color=\"#FFFFFF\">" + textTitleUnder + "</font>")));
        }
    }

    public final void showWaitDialog() {
        progressDialog = new ProgressDialog(new ContextThemeWrapper(this, android.R.style.Theme_Holo_Light_Dialog));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(getText(R.string.app_wait_dialog_message));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public final void dismissWaitDialog() {
        if (progressDialog != null) {
            try {
                progressDialog.dismiss();
            } catch (Exception e) {
            }
        }
    }

    protected abstract void onCreateView();

    protected abstract void getData();
}