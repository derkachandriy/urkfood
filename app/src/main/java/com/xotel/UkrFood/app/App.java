package com.xotel.UkrFood.app;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.ContextThemeWrapper;

import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.xotel.UkrFood.R;
import com.xotel.UkrFood.activities.AcAuth;
import com.xotel.UkrFood.dialogs.DlgError;
import com.xotel.UkrFood.dialogs.DlgInternet;
import com.xotel.UkrFood.dialogs.DlgNeedAuthorization;
import com.xotel.msb.apilib.Api;
import com.xotel.msb.apilib.ApiMessages;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 22.07.14
 * Time: 13:14
 */
public class App extends Application implements DlgInternet.InternetCallBack {
    private Api mApi;
    private static ProgressDialog pd;
    private static final int MSG_TIMEOUT_PRELOADER = 1;
    private Context mTmpContext;
    private boolean mIsShowProgressDlg = true;
    public static CoreData coreData;
    public boolean waitForNet = false;
    public Context currentContext;
    private ApiMessages mApiMessages;
    private boolean isProcessError = true;
    public ImageLoader imageLoader;

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == MSG_TIMEOUT_PRELOADER) {
                dismissWaitDialog();
            }
        }
    };

    public Api getApi(Context context) {
        return getApi(context, true, true);
    }

    public Api getApi(Context context, boolean isProcessError) {
        return getApi(context, true, isProcessError);
    }

    public Api getApi(Context context, boolean isShowProgressDlg, boolean isProcessError) {
        this.isProcessError = isProcessError;
        if (mTmpContext == null || mTmpContext != context) {
            pd = new ProgressDialog(new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog));
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setMessage(getText(R.string.app_wait_dialog_message));
            pd.setCancelable(false);
            pd.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    if (mHandler.hasMessages(MSG_TIMEOUT_PRELOADER))
                        mHandler.removeMessages(MSG_TIMEOUT_PRELOADER);
                }
            });
        }
        mTmpContext = context;
        mIsShowProgressDlg = isShowProgressDlg;

        return mApi;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        coreData = new CoreData(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mApi = new Api(this, getResources().getString(R.string.app_id), getResources().getInteger(R.integer.hotel_id),
                coreData.getPreferences().getLang(), 29, new com.xotel.framework.network.Api.ApiEventListener<ApiMessages>() {
            @Override
            public void onApiStartRequest() {
                if (mIsShowProgressDlg)
                    try {
                        mHandler.sendEmptyMessageDelayed(MSG_TIMEOUT_PRELOADER, 30000);
                        showWaitDialog();
                    } catch (Exception e) { /**/ }
            }

            @Override
            public void onApiFinishRequest() {
                if (mIsShowProgressDlg)
                    dismissWaitDialog();
            }

            @Override
            public void onApiError(final ApiMessages api) {
                mApiMessages = api;
                if (mIsShowProgressDlg)
                    dismissWaitDialog();
                if (api != null) {
                    if (api.getLastServerError().getResponseCodeType() == com.xotel.framework.network.Message.ErrorCode.CODE_NETWORK_ACCESS) {
                        if (isProcessError)
                            new DlgInternet(currentContext, App.this).show();
                    } else if (api.getLastServerError().getResponseCodeType() == com.xotel.framework.network.Message.ErrorCode.CODE_SERVER_ERROR) {
                        switch (api.getLastServerError().getResponseFailCode()) {
                            case 1000: {
                                new DlgError(currentContext, api.getLastServerError().getResposneFailMassage()).show();
                                break;
                            }
                            default: {
                                int resId = getResources().getIdentifier("error_" + api.getLastServerError().getResponseFailCode(), "string", getPackageName());
                                if (resId != 0)
                                    new DlgError(currentContext, getResources().getString(resId)).show();
                                else
                                    new DlgError(currentContext, api.getLastServerError().getResposneFailMassage()).show();
                                break;
                            }
                        }
                    } else if (api.getLastServerError().getResponseCodeType() == com.xotel.framework.network.Message.ErrorCode.CODE_WRONG_ANSWER) {
                        new DlgError(currentContext, getString(R.string.parser_error), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                System.exit(0);
                            }
                        }).show();
                    }
                } else
                    new DlgError(currentContext, getString(R.string.parser_error), null).show();
            }
        });
        mApi.getSession().setServerAddress(getString(R.string.server_address));
        mApi.getSession().setAccess_token(coreData.getPreferences().getAccessToken());
        mApi.getSession().setLang(coreData.getPreferences().getLang());

        imageLoader = ImageLoader.getInstance();
        ImageLoaderConfiguration api16config = new ImageLoaderConfiguration.Builder(this)
                .threadPoolSize(4)
                .threadPriority(Thread.NORM_PRIORITY + 2)
                .denyCacheImageMultipleSizesInMemory()
                .offOutOfMemoryHandling()
                .memoryCache(new WeakMemoryCache())
                .discCacheFileNameGenerator(new HashCodeFileNameGenerator())
                .defaultDisplayImageOptions(getDisplayImageOptions(R.drawable.no_image_slider))
                .build();

        imageLoader.init(api16config);
    }

    public static DisplayImageOptions getDisplayImageOptions(int no_image_res) {
        return new DisplayImageOptions.Builder()
                .cacheInMemory()
                .cacheOnDisc()
                .showStubImage(no_image_res)
                .showImageForEmptyUri(no_image_res)
                .build();
    }

    public final void needAuthorization() {
        new DlgNeedAuthorization(currentContext, getString(R.string.need_authorization), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(currentContext, AcAuth.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                currentContext.startActivity(intent);
            }
        }).show();
    }

    private void showWaitDialog() {
        if (!pd.isShowing()) {
            pd.show();
        }
    }

    private void dismissWaitDialog() {
        if (pd != null) {
            try {
                pd.dismiss();
                mHandler.removeMessages(MSG_TIMEOUT_PRELOADER);
            } catch (Exception e) {
            }
        }
    }

    public void aliveApiMsgManual() {
        if (mApiMessages != null) {
            ApiMessages a = mApiMessages;
            mApiMessages = null;
            a.wakeNetMsgManual();
        }
    }

    @Override
    public void onSkipInternetAccess() {
        aliveApiMsgManual();
    }

    @Override
    public void onSetupInternetAccess() {
        waitForNet = true;
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(android.provider.Settings.ACTION_SETTINGS);
        startActivity(intent);
    }

    public void aliveApiMsg() {
        if (mApiMessages != null) {
            ApiMessages a = mApiMessages;
            mApiMessages = null;
            a.wakeNetMsg();
        }
    }

    public boolean hasApiProblem() {
        return !(mApiMessages == null || !mApiMessages.hasNetProblem());
    }
}