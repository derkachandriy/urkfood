package com.xotel.UkrFood.app;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16.09.14
 * Time: 15:28
 */
public final class Preferences {
    private final SharedPreferences.Editor mEditor;
    private final SharedPreferences mSharedPreferences;
    public static final String PREFERENCES = "coreDataUkrFood";
    public static final String DEF_LANG = "uk";
    private static final String F_LANG = "lang";
    private static final String F_IS_AUTHORIZED = "is_authorized";
    private static final String F_USER_LOGIN = "user_login";
    private static final String F_USER_ID = "user_id";
    public static final String PROPERTY_REG_PUSH_ID = "registration_id";
    private final String ACCESS_TOKEN = "access_token";
    private static final String F_IS_FIRST = "is_first";

    public Preferences(Context context) {
        mSharedPreferences = context.getSharedPreferences(PREFERENCES, 0);
        mEditor = mSharedPreferences.edit();
    }

    public String getLang() {
        return mSharedPreferences.getString(F_LANG, DEF_LANG);
    }

    public void setLang(String lang) {
        mEditor.putString(F_LANG, lang);
        mEditor.commit();
    }

    public boolean isAuthorized() {
        return mSharedPreferences.getBoolean(F_IS_AUTHORIZED, false);
    }

    public void setAuthorized(boolean isAuthorized) {
        mEditor.putBoolean(F_IS_AUTHORIZED, isAuthorized).commit();
    }

    public void setUserId(String userId) {
        mEditor.putString(F_USER_ID, userId).commit();
    }

    public String getUserId() {
        return mSharedPreferences.getString(F_USER_ID, "");
    }

    public String getUserLogin() {
        return mSharedPreferences.getString(F_USER_LOGIN, "");
    }

    public void setUserLogin(String userLogin) {
        mEditor.putString(F_USER_LOGIN, userLogin).commit();
    }

    public void setPropertyRegPushId(String propertyRegPushId) {
        mEditor.putString(PROPERTY_REG_PUSH_ID, propertyRegPushId);
        mEditor.commit();
    }

    public String getPropertyRegPushId() {
        return mSharedPreferences.getString(PROPERTY_REG_PUSH_ID, "");
    }

    public String getAccessToken() {
        return mSharedPreferences.getString(ACCESS_TOKEN, null);
    }

    public void setAccessToken(String accessToken) {
        mEditor.putString(ACCESS_TOKEN, accessToken);
        mEditor.commit();
    }

    public void setIsFirst(boolean isFirst) {
        mEditor.putBoolean(F_IS_FIRST, isFirst).commit();
    }

    public boolean isFirst() {
        return mSharedPreferences.getBoolean(F_IS_FIRST, true);
    }
}