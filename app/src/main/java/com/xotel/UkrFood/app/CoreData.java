package com.xotel.UkrFood.app;

import android.content.Context;

/**
 * Created with IntelliJ IDEA.
 * User: Derkach A.
 * Date: 21.08.14
 * Time: 14:56
 */
public final class CoreData {
    private Preferences mPreferences;

    public CoreData(Context context) {
        mPreferences = new Preferences(context);
    }

    public Preferences getPreferences() {
        return mPreferences;
    }

    public void clearPreferences() {
        mPreferences.setLang(null);
        mPreferences.setAccessToken(null);
        mPreferences.setAuthorized(false);
        mPreferences.setPropertyRegPushId(null);
        mPreferences.setUserId(null);
        mPreferences.setUserLogin(null);
        mPreferences.setIsFirst(false);
    }
}