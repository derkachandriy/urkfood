package com.xotel.UkrFood.app;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xotel.msb.apilib.Api;

/**
 * Created by android on 07.07.15.
 */
@SuppressLint("ValidFragment")
public abstract class BaseFragment extends Fragment {
    protected BaseActivity activity;
    protected CoreData coreData;
    protected View view;

    public BaseFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.activity = (BaseActivity) getActivity();
        assert activity != null;
        coreData = activity.coreData;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        onCreateView(inflater);
        getData();

        return view;
    }

    protected Api getApi() {
        return activity.getApi(activity);
    }

    protected void runOnUiThread(Runnable runnable) {
        activity.runOnUiThread(runnable);
    }

    protected android.support.v4.app.FragmentManager getSupportFragmentManager() {
        return activity.getSupportFragmentManager();
    }

    public final void setTitleUnder(String title) {
        activity.setTitleUnder(title);
    }

    protected View findViewById(int id) {
        return view.findViewById(id);
    }

    protected void setTitle(String msg) {
        activity.setTitle(msg);
    }

    protected void setTitle(int resId) {
        activity.setTitle(resId);
    }

    public void startActivity(Intent intent) {
        activity.startActivity(intent);
    }

    protected abstract void onCreateView(LayoutInflater inflater);

    protected abstract void getData();
}