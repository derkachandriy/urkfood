package com.xotel.UkrFood.app;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 08.09.14
 * Time: 12:47
 */
public class ExtraMsg {
    public static final String E_MSG_ICON_URLS = "icon_urls";
    public static final String E_MSG_OBJECT_ID = "object_id";
    public static final String E_MSG_GROUP_ID = "group_id";
    public static final String E_MSG_CURR_IMAGE = "curr_image";
    public static final String E_MSG_CURRENT_ITEM = "current_item";
    public static final String E_MSG_OBJECT = "object";
    public static final String E_MSG_FLAG = "flag";
    public static final String E_MSG_FLAG_UPDATE = "flag_update";
}